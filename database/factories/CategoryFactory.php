<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Category::class, function (Faker $faker) {
    $title = $faker->word;
    $slug = str_slug($title, '-');
    return [
      'categoryname' => $title,
      'description' => $faker->paragraph,
      'slug' => $slug,
    ];
});
