<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

function generatesku() {
  $number = mt_rand(1000000000, 9999999999);
  if (skuExists($number)) {
      return generatesku();
  }
  return $number;
}
function skuExists($number)
{
  return Product::wheresku($number)->exists();
}
$factory->define(Product::class, function (Faker $faker) {
    $title = $faker->word;
    $slug = str_slug($title, '-');
    return [
        'name' => $title,
        'price' => $faker->randomFloat,
        'description' => $faker->paragraph,
        'sku' => generatesku(),
        'quantity' => $faker->randomDigit ,
        'slug' => $slug,
        'createdby' => 9,
    ];
});
