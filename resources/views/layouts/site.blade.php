<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <!--link rel="icon" href="#"-->

        <title>{{config('app.name')}}</title>

        <!-- Bootstrap core CSS -->
        <link href="/site-template/assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="/site-template/css/theme.min.css" rel="stylesheet">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:500,800" rel="stylesheet">

        <!-- Icons -->
        <link href="/site-template/assets/fonts/icofont/icofont.min.css" rel="stylesheet">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="/admin-lte/plugins/fontawesome-free/css/all.min.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <!--
        PRELOADER
        =============================================== -->
        <!-- <div class="preloader">
            <img src="/site-template/images/preloader.gif" alt="">
        </div> -->
        <!-- END: PRELOADER -->

        <!--
        NAVBAR
        =============================================== -->
        @include('site.navbar')
        <!-- END: NAVBAR -->
        @yield('breadcrumb')
        @yield('content')
        <!--
        FOOTER
        =============================================== -->
        @include('site.footer')
        <!-- END: FOOTER -->

        <!--
        REGISTER MODAL
        =============================================== -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">
                                <i class="icofont icofont-close-line"></i>
                            </span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">
                            Authorization
                            <span>
                                required
                            </span>
                        </h4>
                    </div>

                    <div class="modal-body">

                        <!-- Authirize form -->
                        <div class="row auth-form">
                            <div class="col-md-4">
                                <!-- Nav -->
                                <div class="asside-nav no-bg">
                                    <ul class="nav-vrt border">
                                        <li class="active">
                                            <a href="#" class="btn-material">Privacy policy</a>
                                        </li>
                                        <li>
                                            <a href="#" class="btn-material">Terms and conditions</a>
                                        </li>
                                        <li>
                                            <a href="#" class="btn-material">FAQ</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-5 col-md-offset-1 form-fields">
                              <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group mb-3">
                                      <label for="email">Email</label>
                                      <input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                      <div class="input-group-append">
                                        <div class="input-group-text">
                                        </div>
                                      </div>
                                      @error('email')
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                          </span>
                                      @enderror
                                    </div>

                                    <div class="form-group mb-3">
                                      <input placeholder="password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                      @error('password')
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                          </span>
                                      @enderror
                                      <div class="input-group-append">
                                        <div class="input-group-text">
                                        </div>
                                      </div>
                                    </div>

                                    <div class="checkbox padding">
                                      <input  type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label for="remember">
                                            <span class="checkbox-input">
                                            <span class="off">off</span>
                                            <span class="on">on</span>
                                            </span>
                                            Remember Password
                                        </label>
                                    </div>
                                    <span class="sdw-wrap">
                                        <button type="submit" class="sdw-hover btn btn-material btn-yellow btn-lg ripple-cont">  {{ __('Login') }}</button>
                                        <span class="sdw"></span>
                                    </span>

                                    <ul class="addon-login-btn">
                                        <li>
                                            <a href="{{ route('register') }}">Register</a>
                                        </li>
                                        <li>or</li>
                                        <li>
                                            <a href="{{ route('password.request') }}">Forgot Password?</a>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                        <!-- / Authirize form -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END: REGISTER MODAL -->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="/site-template/assets/js/jquery.min.js"></script>
        <script src="/site-template/assets/js/bootstrap.min.js"></script>
        <script src="/site-template/assets/js/jquery-ui.min.js"></script>
        <script src="/site-template/assets/js/owl.carousel.min.js"></script><!-- OWL Carousel -->
        <script src="/site-template/assets/js/lv-ripple.jquery.min.js"></script><!-- BTN Material effects -->
        <script src="/site-template/assets/js/SmoothScroll.min.js"></script><!-- SmoothScroll -->
        <script src="/site-template/assets/js/jquery.TDPageEvents.min.js"></script><!-- Page Events -->
        <script src="/site-template/assets/js/jquery.TDParallax.min.js"></script><!-- Parallax -->
        <script src="/site-template/assets/js/jquery.TDTimer.min.js"></script><!-- Timer -->
        <script src="/site-template/assets/js/selectize.min.js"></script><!-- Select customize -->
        <script src="/site-template/js/main.min.js"></script>
        @yield('script')
    </body>

</html>
