
<nav class="navbar navbar-default">

    <div class="container">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/site-template/images/main-brand.png" alt="" class="brand">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <!-- Top panel / search / phone -->
            <div class="top-panel">

                <div class="phone text-blue">
                    <i class="icofont icofont-phone-circle"></i>
                    {{env('CONTACT_1')}}
                </div>


                <div class="btn-cols">


                      @auth
                      <ul class="list-btn-group">
                      <li>
                        <a style="border-radius: 3px;" href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                              Logout
                          </a>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                          </form>
                      </li>
                      </ul>
                      @else
                      <ul class="list-btn-group">

                      <li>
                          <a href="#" data-toggle="modal" data-target="#myModal">
                              Sign In
                          </a>
                      </li>

                      <li>
                          <a href="{{ route('register') }}">
                              <b>Register</b>
                          </a>
                      </li></ul>
                      @endauth


                </div>
            </div>

            <ul class="nav navbar-nav navbar-right info-panel">
                @auth
                <!-- Profile -->
                <li class="profile">
                    <span class="wrap">

                        <!-- Image -->
                        <span class="image bg-white">

                            <!-- New message badge -->
                            <span class="badge bg-blue hidden-xs hidden-sm">5</span>

                            <a href="/dashboard">
                              <span class="icon">
                                  <i class="icofont icofont-user-alt-4 text-blue"></i>
                              </span>
                            </a>

                            <!--img src="/site-template/images/profile/profile-img.jpg" alt=""-->
                        </span>

                        <!-- Info -->
                        <span class="info">
                            <!-- Name -->

                            <span class="name text-uppercase">{{Auth::user()->name}}</span>
                            <a href="/dashboard">My Account</a>
                        </span>
                    </span>
                </li>
                @endauth
                <!-- Cart -->
                <li class="cart">

                    <a href="#" class="cart-icon hidden-xs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">

                        <span class="badge bg-blue">3</span>

                        <i class="icofont icofont-cart-alt"></i>
                    </a>

                    <a href="#" class="visible-xs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="icofont icofont-cart-alt"></i>
                        Shopping cart
                    </a>

                    <!-- Dropdown items list -->
                    <ul class="dropdown-menu">

                        <!-- Item -->
                        <li>
                            <div class="wrap">

                                <!-- Image -->
                                <div class="image">
                                    <img src="/site-template/images/shop/img-01.jpg" alt="">
                                </div>

                                <!-- Caption -->
                                <div class="caption">
                                    <span class="comp-header st-1 text-uppercase">
                                        T-SHIPT
                                        <span>
                                            MEN COLLECTION
                                        </span>
                                        <span>
                                            FAKE BRAND
                                        </span>
                                    </span>

                                    <span class="price">
                                        <span class="text-grey-dark">Rs</span>
                                        257 <small class="text-grey-dark">.00</small>
                                    </span>
                                </div>

                                <!-- Remove btn -->
                                <span class="remove-btn bg-blue">
                                    <i class="icofont icofont-bucket"></i>
                                </span>
                            </div>
                        </li>

                        <!-- Item -->
                        <li>
                            <div class="wrap">

                                <!-- Image -->
                                <div class="image">
                                    <img src="/site-template/images/shop/img-01.jpg" alt="">
                                </div>

                                <!-- Caption -->
                                <div class="caption">
                                    <span class="comp-header st-1 text-uppercase">
                                        T-SHIPT
                                        <span>
                                            MEN COLLECTION
                                        </span>
                                        <span>
                                            FAKE BRAND
                                        </span>
                                    </span>

                                    <span class="price">
                                        <span class="text-grey-dark">Rs</span>
                                        257 <small class="text-grey-dark">.00</small>
                                    </span>
                                </div>

                                <!-- Remove btn -->
                                <span class="remove-btn bg-blue">
                                    <i class="icofont icofont-bucket"></i>
                                </span>
                            </div>
                        </li>

                        <!-- Item -->
                        <li>
                            <div class="wrap">

                                <!-- Image -->
                                <div class="image">
                                    <img src="/site-template/images/shop/img-01.jpg" alt="">
                                </div>

                                <!-- Caption -->
                                <div class="caption">
                                    <span class="comp-header st-1 text-uppercase">
                                        T-SHIPT
                                        <span>
                                            MEN COLLECTION
                                        </span>
                                        <span>
                                            FAKE BRAND
                                        </span>
                                    </span>

                                    <span class="price">
                                        <span class="text-grey-dark">Rs</span>
                                        257 <small class="text-grey-dark">.00</small>
                                    </span>
                                </div>

                                <!-- Remove btn -->
                                <span class="remove-btn bg-blue">
                                    <i class="icofont icofont-bucket"></i>
                                </span>
                            </div>
                        </li>


                       <li class="more-btn sdw">
                           <a href="/cart" class="btn-material btn-primary">
                               View order <i class="icofont icofont-check-circled"></i>
                           </a>
                       </li>


                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav">
                <li class="">
                    <a href="/">
                        Home
                    </a>
                </li>
                <li class="">
                    <a href="/shop">
                        Products
                    </a>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Categories <i class="icofont icofont-curved-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                      @foreach(DB::table('categories')->whereNull('parent_id')->get() as $category)
                        <li><a href="{{'/category/'. $category->slug}}">{{$category->categoryname}}</a></a></li>
                      @endforeach
                    </ul>
                </li>
                <li>
                    <a href="/contact">
                        Contact Us
                    </a>
                </li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->

</nav>
