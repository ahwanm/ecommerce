@extends('layouts.site')
@section('content')
        <div class="container space-top space-bottom">

            <!--
            STEPS
            =============================================== -->
            <div class="row block none-padding-top">
                <div class="col-xs-12">

                    <ul class="steps row">
                        <li class="hidden-xs col-xs-12 col-sm-4 col-md-4 col-lg-3">
                            <div class="icon number bg-blue">
                                1
                            </div>
                            <span>
                                Confirm
                            </span>
                            products list

                            <span class="dir-icon">
                                <i class="icofont icofont-stylish-right"></i>
                            </span>
                        </li>

                        <li class="col-sm-4 col-md-4 col-lg-3">
                            <div class="icon number bg-blue">
                                2
                            </div>
                            <span>
                                Enter
                            </span>
                            your address

                            <span class="dir-icon hidden-xs">
                                <i class="icofont icofont-stylish-right text-yellow"></i>
                            </span>
                        </li>

                        <li class="hidden-xs col-sm-4 col-md-4 col-lg-3">
                            <div class="icon number bg-grey">
                                3
                            </div>
                            <span>
                                Select
                            </span>
                            payment method

                            <span class="dir-icon hidden-sm hidden-md">
                                <i class="icofont icofont-stylish-right"></i>
                            </span>
                        </li>

                        <li class="hidden-xs col-lg-3 hidden-sm hidden-md">
                            <div class="icon number bg-grey">
                                4
                            </div>
                            <span>
                                Confirm
                            </span>
                            your order
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END: STEPS -->


            <!--
            CONTENT
            =============================================== -->
            <div class="row block none-padding-top">

                <div class="col-xs-12 col-md-8 col-lg-9 get-height">
                    <div class="sdw-block">
                        <div class="wrap bg-white">
                            @guest
                              <!-- Authirize form -->

                              <div class="row auth-form">

                                  <!-- Header & nav -->
                                  <div class="col-md-12">

                                      <!-- Header -->
                                      <h1 class="header text-uppercase">
                                          Authirization
                                          <span>
                                              required
                                          </span>
                                      </h1>

                                  </div>

                                  <div class="col-md-4">
                                      <!-- Text -->
                                      <span class="text">
                                          Magni labore ratione maiores, laborum quaerat molestiae excepturi. Corporis, necessitatibus earum.
                                      </span>

                                      <!-- Nav -->
                                      <div class="asside-nav no-bg">
                                          <ul class="nav-vrt border">
                                              <li class="active">
                                                  <a href="#" class="btn-material">Privacy policy</a>
                                              </li>

                                              <li>
                                                  <a href="#" class="btn-material">Terms and conditions</a>
                                              </li>

                                              <li>
                                                  <a href="#" class="btn-material">FAQ</a>
                                              </li>
                                          </ul>
                                      </div>
                                  </div>

                                  <div class="col-md-6 col-md-offset-1">
                                    <form method="POST" action="{{ route('login') }}">
                                      @csrf
                                      <div class="form-group mb-3">
                                        <label for="email">Email</label>
                                        <input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        <div class="input-group-append">
                                          <div class="input-group-text">
                                          </div>
                                        </div>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                      </div>

                                      <div class="form-group mb-3">
                                        <input placeholder="password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <div class="input-group-append">
                                          <div class="input-group-text">
                                          </div>
                                        </div>
                                      </div>

                                      <div class="checkbox padding">
                                        <input  type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                          <label for="remember">
                                              <span class="checkbox-input">
                                              <span class="off">off</span>
                                              <span class="on">on</span>
                                              </span>
                                              Remember Password
                                          </label>
                                      </div>
                                      <span class="sdw-wrap">
                                          <button type="submit" class="sdw-hover btn btn-material btn-yellow btn-lg ripple-cont">  {{ __('Login') }}</button>
                                          <span class="sdw"></span>
                                      </span>

                                      <ul class="addon-login-btn">
                                          <li>
                                              <a href="{{ route('register') }}">Register</a>
                                          </li>
                                          <li>or</li>
                                          <li>
                                              <a href="{{ route('password.request') }}">Forgot Password?</a>
                                          </li>
                                      </ul>
                                    </form>
                                  </div>
                              </div>
                              <!-- / Authirize form -->
                            @endguest
                            @auth
                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="">
                                            <div id="collapseTwo" class="panel-collapse collapse in">
                                                <div class="panel-body">

                                                    <form class="form-horizontal">

                                                        <!-- Authocompille -->
                                                        <div class="form-group pd-none">
                                                            <label for="autocomplete" class="col-sm-3 control-label text-darkness">Address</label>
                                                            <div class="col-sm-8">
                                                                <input type="text"
                                                                       class="form-control"
                                                                       id="autocomplete"
                                                                       onFocus="geolocate()">
                                                            </div>
                                                        </div>


                                                        <div class="form-group pd-none">
                                                            <label for="locality" class="col-sm-3 control-label text-darkness">City</label>
                                                            <div class="col-sm-8">
                                                                <input type="text"
                                                                       class="form-control"
                                                                       id="locality"
                                                                       >
                                                            </div>
                                                        </div>

                                                        <div class="form-group pd-none">
                                                            <label for="administrative_area_level_1" class="col-sm-3 control-label text-darkness">State</label>
                                                            <div class="col-sm-8">
                                                                <input type="text"
                                                                       class="form-control"
                                                                       id="administrative_area_level_1"
                                                                       >
                                                            </div>
                                                        </div>

                                                        <div class="form-group pd-none">
                                                            <label for="postal_code" class="col-sm-3 control-label text-darkness">Zip code</label>
                                                            <div class="col-sm-8">
                                                                <input type="text"
                                                                       class="form-control"
                                                                       id="postal_code"
                                                                       >
                                                            </div>
                                                        </div>

                                                        <div class="form-group pd-none">
                                                            <label for="country" class="col-sm-3 control-label text-darkness">Country</label>
                                                            <div class="col-sm-8">
                                                                <input type="text"
                                                                       class="form-control"
                                                                       id="country"
                                                                       >
                                                            </div>
                                                        </div>
                                                        <!-- / Authocompille -->


                                                        <div class="form-group" style="margin-top: 10px">
                                                            <div class="col-sm-offset-3 col-sm-8">
                                                                <span class="sdw-wrap">
                                                                    <a href="/checkout" class="sdw-hover btn btn-material btn-yellow btn-lg ripple-cont">Payment</a>
                                                                    <span class="sdw"></span>
                                                                </span>
                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            @endauth

                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4 col-lg-3 fix-height asside hidden-xs hidden-sm">
                    <div class="my-list float-block">
                        <div class="wrap bg-white" style="padding-top: 0">

                                <!-- Asside nav -->
                                <div class="asside-nav bg-grey-lightness hidden-xs">
                                    <div class="header text-uppercase text-white bg-blue">
                                        Order Summary
                                    </div>

                                    <ul class="list-2">
                                        <li>
                                            <span class="head">Number of items:</span>
                                            <span class="sub">09</span>
                                        </li>
                                        <li>
                                            <span class="head">Discount:</span>
                                            <span class="sub">Rs 20.00</span>
                                        </li>
                                        <li>
                                            <span class="head">Total price:</span>
                                            <span class="sub">Rs 2,250.00</span>
                                        </li>
                                    </ul>

                                    <div class="asside-btn text-center">
                                        <a href="#" class="btn btn-primary btn-material">
                                            <span class="body">Edit order</span>
                                            <i class="icon icofont icofont-check-circled"></i>
                                        </a>
                                    </div>

                                </div><!-- / Asside nav -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- END: CONTENT -->

        </div>
@endsection
