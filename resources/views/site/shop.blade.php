@extends('layouts.site')
@section('content')
<!--
BLOCK POPULAR ON SHOP
=============================================== -->
<div class="container-fluid block">
    <div class="row">

            <div class="container">
                <div class="row">

                    <!-- Item list -->
                    <div class="col-md-8 col-lg-9 shop-items-set">

                        <!-- Paginations -->
                        <div class="row pagination-block hidden-xs">
                            <div class="col-xs-12">

                                <div class="wrap">
                                    <!-- Pagination -->
                                    <ul class="pagination">
                                        <li>
                                            <a href="{{route('site-shop', [ 'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category])}}">
                                                <span><i class="icofont icofont-rounded-left"></i></span>
                                            </a>
                                        </li>
                                        @for ($i = 1; $i <= ceil($products->total()/$count); $i++)
                                            <li @if($i==request()->page || (request()->page == null && $i==1)) class='active' @endif><a href="{{route('site-shop',['page'=>$i, 'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category ])}}">{{$i}}</a></li>
                                        @endfor

                                        <li>
                                            <a href="{{route('site-shop',['page'=>ceil($products->total()/$count),  'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category])}}">
                                                <span><i class="icofont icofont-rounded-right"></i></span>
                                            </a>
                                        </li>
                                    </ul>

                                </div>

                            </div>
                        </div>

                        <!-- Item list -->
                        <div class="row item-wrapper">
                            @if($products->all()==[])
                              <div class="col-xs-6">
                                <p>No Products Found</p>
                              </div>
                            @endif
                            @foreach ($products as $product)
                            <!-- Shop item  -->
                            <div class="col-xs-6 col-sm-4 col-md-6 col-lg-4 shop-item hover-sdw timer"
                                 data-timer-date="2018, 2, 5, 0, 0, 0">

                                <div class="wrap">

                                    <!-- Image & Caption -->
                                    <div class="body">

                                        <!-- Header -->
                                        <div class="comp-header st-4 text-uppercase">

                                            {{$product->name}}
                                            <span>
                                                {{ str_limit(implode(', ' , $product->categories->pluck('categoryname')->all()), $limit = 15, $end = '...') }}
                                            </span>

                                        </div>

                                        <!-- Image -->
                                        <div class="image">
                                            <img class="main" src="{{ $product->getFirstMediaUrl('productImages','thumb') }}" alt="">
                                        </div>

                                        <!-- Caption -->
                                        <div class="caption">

                                            <!-- Features list -->
                                            <ul class="features">
                                                <li>
                                                    <i class="icofont icofont-shield"></i>
                                                    <span>24 days. Money Back Guarantee</span>
                                                </li>
                                                <li>
                                                    <i class="icofont icofont-ship"></i>
                                                    <span>Free shipping</span>
                                                </li>
                                                <li>
                                                    <i class="icofont icofont-hand"></i>
                                                    <span>Free help and setup</span>
                                                </li>
                                            </ul>

                                            <!-- Text -->
                                            <p class="text">
                                                Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                            </p>
                                        </div>
                                    </div>

                                    <!-- Buy btn & more link -->
                                    <div class="info">

                                        <!-- Buy btn -->
                                        <a href="#" class="btn-material btn-price">

                                            <!-- Price -->
                                            <span class="price">

                                                <!-- Currency -->
                                                <span class="curr">
                                                    Rs
                                                </span>


                                                <!-- Price -->
                                                <span class="price">
                                                    {{$product->price}}
                                                </span>
                                            </span>

                                            <!-- Quantity -->
                                            <span class="qnt-select">
                                                <span class="plus">
                                                    <i class="icofont icofont-plus"></i>
                                                </span>
                                                <span class="view-sum">
                                                    01
                                                </span>
                                                <span class="minus">
                                                    <i class="icofont icofont-minus"></i>
                                                </span>
                                            </span>

                                            <!-- Icon card -->
                                            <span class="icon-card">
                                                <i class="icofont icofont-cart-alt"></i>
                                            </span>
                                        </a>


                                        <!-- More link -->
                                        <a href="{{'/product/'. $product->slug . '/' . $product->sku}}" class="more-link">More info</a>
                                    </div>
                                </div>
                            </div>
                            <!-- / Shop item -->

                            @endforeach

                        </div>

                        <!-- Paginations -->
                        <div class="row pagination-block bottom">
                            <div class="col-xs-12">

                                <div class="wrap">
                                    <!-- Pagination -->
                                    <ul class="pagination">
                                        <li>
                                            <a href="{{route('site-shop', [ 'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category])}}">
                                                <span><i class="icofont icofont-rounded-left"></i></span>
                                            </a>
                                        </li>
                                        @for ($i = 1; $i <= ceil($products->total()/$count); $i++)
                                            <li @if($i==request()->page || (request()->page == null && $i==1)) class='active' @endif><a href="{{route('site-shop',['page'=>$i, 'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category ])}}">{{$i}}</a></li>
                                        @endfor

                                        <li>
                                            <a href="{{route('site-shop',['page'=>ceil($products->total()/$count),  'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category])}}">
                                                <span><i class="icofont icofont-rounded-right"></i></span>
                                            </a>
                                        </li>
                                    </ul>

                                </div>

                            </div>
                        </div>
                    </div><!-- ./ Item list -->

                    <!-- Asside -->
                    <div class="col-md-4 col-lg-3 asside">

                        <!-- Block setup -->
                        <div class="inblock sdw">
                            <div class="wrap bg-white">
                              <form class="" action="/shop" method="get">

                                  <!-- Header -->
                                  <h3 class="header text-uppercase">Price</h3>

                                  <!-- Price amount -->
                                  <div class="price-slider"
                                       data-price-first=@if(request()->has('priceLow')){{explode(' ', request()->priceLow)[1]}} @else  0 @endif
                                       data-price-last=@if(request()->has('priceHigh')){{explode(' ', request()->priceHigh)[1]}} @else {{$maxPrice}} @endif
                                       data-price-max={{$maxPrice}}
                                       data-price-curr="Rs ">

                                      <div class="range"></div>

                                      <div class="amoutn">
                                          <input type="text" id='priceLow' name='priceLow' class="first" readonly>
                                          <input type="text" id='priceHigh' name='priceHigh' class="last" readonly>
                                      </div>
                                  </div><!-- / Price amount -->
                                  <!-- Divider -->
                                  <div class="divider"></div><!-- / Divider -->
                                  <button id="priceBtn" type="submit" class="btn btn-primary btn-material ripple-cont" >
                                    <div class="ripple-content">
                                              <span class="body">Apply</span>
                                    </div></button>

                              </form>
                            </div>
                        </div><!-- Block setup -->

                        <!-- Asside nav -->
                        <div class="asside-nav bg-white hidden-xs">
                            <div class="header text-uppercase text-white bg-blue">
                                Category
                            </div>

                            <ul class="nav-vrt bg-white">
                                @foreach($parentCategories as $parentCategory)
                                <li @if((request()->category == $parentCategory->id) || (in_array(request()->category, $parentCategory->children->pluck('id')->all()))) class='active' @endif>
                                    <a href="{{route('site-shop',['category'=> $parentCategory->id])}}" class="btn-material">{{$parentCategory->categoryname}}
                                      @if($parentCategory->children->all() !=[])
                                        <i class="nav-icon-open icofont icofont-plus"></i>
                                        <i class="nav-icon-close icofont icofont-minus"></i>
                                    </a>
                                    <div class="sub-nav bg-grey-light">
                                        <ul class="sub">
                                            @foreach($parentCategory->children->all() as $childCategory)
                                              <li >
                                                  <a href="{{route('site-shop',['category'=> $childCategory->id])}}" class="btn-material">{{$childCategory->categoryname}}</a>
                                              </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @else
                                      </a>
                                    @endif
                                </li>
                                @endforeach

                            </ul>

                        </div><!-- / Asside nav -->


                        <!-- List categories for mobile -->
                        <div class="inblock padding-none visible-xs">
                            <div class="mobile-category nav-close">

                                <!-- Header -->
                                <div class="header bg-blue">
                                    <span class="head">Category</span>

                                    <span class="btn-swither" >
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </span>
                                </div>

                                <ul class="nav-vrt bg-white">
                                    <li class="active">
                                        <a href="#" class="btn-material">Man line
                                            <i class="nav-icon-open icofont icofont-plus"></i>
                                            <i class="nav-icon-close icofont icofont-minus"></i>
                                        </a>

                                        <div class="sub-nav bg-grey-light">
                                            <ul class="sub">
                                                <li>
                                                    <a href="#" class="btn-material">Shirts</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Pants</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Footwear</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Belts</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Bags</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Accessories</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Perfume</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Jewerly</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a href="#" class="btn-material">Woman
                                            <i class="nav-icon-open icofont icofont-plus"></i>
                                            <i class="nav-icon-close icofont icofont-minus"></i>
                                        </a>

                                        <div class="sub-nav bg-grey-light">
                                            <ul class="sub">
                                                <li>
                                                    <a href="#" class="btn-material">Shirts</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Pants</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Footwear</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Belts</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Bags</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Accessories</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Perfume</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Jewerly</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a href="#" class="btn-material">Jewerly</a>
                                    </li>

                                    <li>
                                        <a href="#" class="btn-material">Electronics</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div><!-- ./ Asside -->
                </div>
            </div>

    </div><!-- / Parallax wrapper -->
</div>
<!-- END: POPULAR ON SHOP -->

@endsection
