@extends('layouts.site')

@section('content')
<div class="container space-top space-bottom">

            <!--
            STEPS
            =============================================== -->
            <div class="row block none-padding-top">
                <div class="col-xs-12">

                    <ul class="steps row">
                        <li class="hidden-xs hidden-sm hidden-md col-xs-12 col-sm-4 col-md-4 col-lg-3">
                            <div class="icon number bg-blue">
                                1
                            </div>
                            <span>
                                Confirm
                            </span>
                            products list

                            <span class="dir-icon">
                                <i class="icofont icofont-stylish-right"></i>
                            </span>
                        </li>

                        <li class="hidden-xs col-sm-4 col-md-4 col-lg-3">
                            <div class="icon number bg-blue">
                                2
                            </div>
                            <span>
                                Enter
                            </span>
                            your address

                            <span class="dir-icon">
                                <i class="icofont icofont-stylish-right"></i>
                            </span>
                        </li>

                        <li class="col-sm-4 col-md-4 col-lg-3">
                            <div class="icon number bg-blue">
                                3
                            </div>
                            <span>
                                Select
                            </span>
                            payment method

                            <span class="dir-icon hidden-xs">
                                <i class="icofont icofont-stylish-right"></i>
                            </span>
                        </li>

                        <li class="hidden-xs col-lg-3">
                            <div class="icon number bg-blue">
                                4
                            </div>
                            <span>
                                Confirm
                            </span>
                            your order
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END: STEPS -->

            <!--
            CONTENT
            =============================================== -->
            <div class="row block none-padding-top">

                <div class="col-xs-12 col-md-12 col-lg-12 get-height">
                    <div class="sdw-block">
                        <div class="wrap bg-white" style="padding:0px">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
                                        <tr>
                                            <td align="center" style="padding: 35px 35px 20px 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                <table style="max-width:600px;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"  >
                                                    <tr>
                                                        <td align="center" style="line-height: 24px; padding-top: 25px;"> <img src="https://img.icons8.com/carbon-copy/100/000000/checked-checkbox.png" width="125" height="120" style="display: block; border: 0px;" /><br>
                                                            <h2 style="line-height: 36px; color: #333333; margin: 0;"> Thank You For Your Order! </h2>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="line-height: 24px; padding-top: 10px;">
                                                            <p style="text-align: center; font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium iste ipsa numquam odio dolores, nam. </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="padding-top: 20px;">
                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                <tr>
                                                                    <td width="75%" align="left" bgcolor="#eeeeee" style=" padding: 10px;"> Order Confirmation # </td>
                                                                    <td width="25%" align="left" bgcolor="#eeeeee" style=" padding: 10px;"> 2345678 </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="75%" align="left" style="padding: 15px 10px 5px 10px;"> Purchased Item (1) </td>
                                                                    <td width="25%" align="left" style="padding: 15px 10px 5px 10px;"> Rs 100.00 </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="75%" align="left" style="padding: 5px 10px;"> Shipping + Handling </td>
                                                                    <td width="25%" align="left" style="padding: 5px 10px;"> Rs 10.00 </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="75%" align="left" style="padding: 5px 10px;"> Sales Tax </td>
                                                                    <td width="25%" align="left" style="padding: 5px 10px;"> Rs 5.00 </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="padding-top: 20px;">
                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                <tr>
                                                                    <td width="75%" align="left" style=" padding: 10px; border-top: 3px solid #eeeeee; border-bottom: 3px solid #eeeeee;"> TOTAL </td>
                                                                    <td width="25%" align="left" style=" padding: 10px; border-top: 3px solid #eeeeee; border-bottom: 3px solid #eeeeee;"> Rs 115.00 </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="100%" valign="top" width="100%" style="padding: 0 35px 35px 35px; background-color: #ffffff;" bgcolor="#ffffff">
                                                <table style="max-width:600px;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
                                                    <tr>
                                                        <td align="center" valign="top" style="font-size:0;">
                                                            <div style="display:inline-block; max-width:50%; min-width:240px; vertical-align:top; width:100%;">
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:300px;">
                                                                    <tr>
                                                                        <td align="left" valign="top" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                            <p style="font-weight: 800;">Delivery Address</p>
                                                                            <p>675 Massachusetts Avenue<br>11th Floor<br>Cambridge, MA 02139</p>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div style="display:inline-block; max-width:50%; min-width:240px; vertical-align:top; width:100%;">
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:300px;">
                                                                    <tr>
                                                                        <td align="left" valign="top" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                                            <p style="font-weight: 800;">Estimated Delivery Date</p>
                                                                            <p>January 1st, 2016</p>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style=" padding: 35px;" class="bg-green" bgcolor="#1b9ba3">
                                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                    <tr>
                                                        <td align="center" style="padding-top: 25px;">
                                                            <h2 style="color: #ffffff; margin: 0;"> Get 30% off your next order. </h2>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 25px 0 15px 0;">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="center" style="border-radius: 5px;" bgcolor="#66b3b7"> <a href="/"  class="bg-light" style="font-size: 18px;  color: #ffffff; text-decoration: none; border-radius: 5px; padding: 15px 30px;  display: block;">Shop Again</a> </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END: CONTENT -->

        </div>
@endsection
