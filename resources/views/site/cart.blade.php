@extends('layouts.site')
@section('content')
        <div class="container space-top space-bottom">
            <!--
            STEPS
            =============================================== -->
            <div class="row block none-padding-top">
                <div class="col-xs-12">

                    <ul class="steps row">
                        <li class="active col-xs-12 col-sm-4 col-md-4 col-lg-3">
                            <div class="icon number bg-green">
                                1
                            </div>
                            <span>
                                Confirm
                            </span>
                            products list

                            <span class="dir-icon ">
                                <i class="icofont icofont-stylish-right text-yellow"></i>
                            </span>
                        </li>

                        <li class="hidden-xs col-sm-4 col-md-4 col-lg-3">
                            <div class="icon number bg-grey">
                                2
                            </div>
                            <span>
                                Enter
                            </span>
                            your address

                            <span class="dir-icon">
                                <i class="icofont icofont-stylish-right"></i>
                            </span>
                        </li>

                        <li class="hidden-xs col-sm-4 col-md-4 col-lg-3">
                            <div class="icon number bg-grey">
                                3
                            </div>
                            <span>
                                Select
                            </span>
                            payment method

                            <span class="dir-icon hidden-sm hidden-md">
                                <i class="icofont icofont-stylish-right"></i>
                            </span>
                        </li>

                        <li class="hidden-xs col-lg-3 hidden-sm hidden-md">
                            <div class="icon number bg-grey">
                                4
                            </div>
                            <span>
                                Confirm
                            </span>
                            your order
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END: STEPS -->

            <!--
            CONTENT
            =============================================== -->
            <div class="row block none-padding-top">
                <div class="col-xs-12">

                    <div class="my-list">
                        <div class="wrap bg-white">

                            <!-- Header -->
                            <div class="list-header text-uppercase">



                                <div class="product hidden-xs hidden-sm">
                                    Product
                                </div>

                                <div class="price hidden-xs hidden-sm">
                                    Price
                                </div>

                                <div class="qnt hidden-xs hidden-sm">
                                    Quantity
                                </div>

                                <div class="total hidden-xs hidden-sm">
                                    Total
                                </div>

                                <div class="rmv hidden-xs hidden-sm">
                                    Remove
                                </div>
                            </div><!-- / Header -->

                            <!-- List body -->
                            <div class="list-body">

                                <!-- Item -->
                                <div class="item">



                                    <div class="product">
                                        <img src="/site-template/images/shop/img-02.jpg" alt="">

                                        <span class="comp-header st-8 text-uppercase">
                                            T-Shirt
                                            <span>
                                                Men collections
                                            </span>
                                            <span>
                                                fake brand
                                            </span>
                                        </span>
                                    </div>

                                    <div class="price hidden-xs">
                                        <span class="price">
                                            <i class="icofont icofont-rupee">Rs</i>
                                            <span class="prc">
                                                <span>520</span><small>.20</small>
                                            </span>
                                        </span>
                                    </div>

                                    <div class="qnt">
                                        <span>
                                            <span class="minus">
                                                <i class="icofont icofont-minus"></i>
                                            </span>
                                            <span class="input">
                                                <input type="text" value="01">
                                            </span>
                                            <span class="plus">
                                                <i class="icofont icofont-plus"></i>
                                            </span>
                                        </span>
                                    </div>

                                    <div class="total">
                                        <i class="icofont icofont-rupee">Rs</i>
                                        <span>520.20</span>
                                    </div>

                                    <div class="rmv text-center">
                                        <button class="remove-btn">
                                            <i class="icofont icofont-close-line"></i>
                                        </button>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item">



                                    <div class="product">
                                        <img src="/site-template/images/shop/img-03.jpg" alt="">

                                        <span class="comp-header st-8 text-uppercase">
                                            T-Shirt
                                            <span>
                                                Men collections
                                            </span>
                                            <span>
                                                fake brand
                                            </span>
                                        </span>
                                    </div>

                                    <div class="price hidden-xs">
                                        <span class="price">
                                            <i class="icofont icofont-rupee">Rs</i>
                                            <span class="prc">
                                                <span>748</span><small>.00</small>
                                            </span>
                                        </span>
                                    </div>

                                    <div class="qnt">
                                        <span>
                                            <span class="minus">
                                                <i class="icofont icofont-minus"></i>
                                            </span>
                                            <span class="input">
                                                <input type="text" value="01">
                                            </span>
                                            <span class="plus">
                                                <i class="icofont icofont-plus"></i>
                                            </span>
                                        </span>
                                    </div>

                                    <div class="total">
                                        <i class="icofont icofont-rupee">Rs</i>
                                        <span>748.00</span>
                                    </div>

                                    <div class="rmv text-center">
                                        <button class="remove-btn">
                                            <i class="icofont icofont-close-line"></i>
                                        </button>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item">




                                    <div class="product">
                                        <img src="/site-template/images/shop/img-05.jpg" alt="">

                                        <span class="comp-header st-8 text-uppercase">
                                            T-Shirt
                                            <span>
                                                Men collections
                                            </span>
                                            <span>
                                                fake brand
                                            </span>
                                        </span>
                                    </div>

                                    <div class="price hidden-xs">
                                        <span class="price">
                                            <i class="icofont icofont-rupee">Rs</i>
                                            <span class="prc">
                                                <span>257</span><small>.20</small>
                                            </span>
                                        </span>
                                    </div>

                                    <div class="qnt">
                                        <span>
                                            <span class="minus">
                                                <i class="icofont icofont-minus"></i>
                                            </span>
                                            <span class="input">
                                                <input type="text" value="02">
                                            </span>
                                            <span class="plus">
                                                <i class="icofont icofont-plus"></i>
                                            </span>
                                        </span>
                                    </div>

                                    <div class="total">
                                        <i class="icofont icofont-rupee">Rs</i>
                                        <span>514.40</span>
                                    </div>

                                    <div class="rmv text-center">
                                        <button class="remove-btn">
                                            <i class="icofont icofont-close-line"></i>
                                        </button>
                                    </div>
                                </div>
                            </div><!-- / List body -->

                            <!-- Footer -->
                            <div class="list-footer bg-green">
                                <a href="/address" class="btn btn-default btn-material">
                                    <i class="icofont icofont-cart-alt"></i>
                                    <span class="body">Make a purchase</span>
                                </a>
                                <a href="#" class="btn btn-text-white hidden-xs hidden-sm">
                                    <span class="body">remove selected</span>
                                </a>
                            </div><!-- / Footer -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: CONTENT -->
        </div>
@endsection
