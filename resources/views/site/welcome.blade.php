@extends('layouts.site')

@section('content')

<!--
SLIDESHOW
=============================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="clearfix">
            <div class="owl-carousel slideshow">

                <!-- Item -->
                <div class="item">
                   <div class="container">
                       <div class="row">

                           <div class="col-sm-12 col-md-5 hidden-xs hidden-sm">

                               <!-- Header -->
                               <h2 class="header text-uppercase text-blue">Sneakers</h2>

                               <!-- Text -->
                               <p>
                                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et maxime vero amet, quisquam nihil! Odit, hic fugiat!
                               </p>

                               <!-- Buttons -->
                               <span class="btn-panel">

                                     <span class="sdw-wrap">
                                         <a href="shop-item.html" class="sdw-hover btn btn-lg btn-material btn-default"><span class="body">More info</span></a>
                                         <span class="sdw"></span>
                                     </span>

                                     <span class="hor-divider"></span>

                                     <span class="sdw-wrap">
                                         <a href="shop-item.html" class="sdw-hover btn btn-lg btn-material btn-primary"><i class="icon icofont icofont-basket"></i><span class="body">Buy now</span></a>
                                         <span class="sdw"></span>
                                     </span>
                               </span>
                           </div>

                           <div class="col-xs-10 col-xs-offset-1 col-md-7 col-md-offset-0">

                               <!-- Image -->
                               <div class="img">
                                   <img src="site-template/images/slideshow/img-01.png" alt="">
                               </div>

                               <!-- Badge -->
                               <span class="sale-badge bg-green text-uppercase">
                                   new
                               </span>

                               <!-- Price -->
                               <span class="price hidden-xs">
                                   <span class="wrap text-red">
                                       Rs 254<small>.50</small>-
                                   </span>
                               </span>

                               <!-- Mobile button -->
                               <span class="text-center visible-xs">
                                    <span class="sdw-wrap">
                                        <a href="#" class="sdw-hover btn btn-lg btn-material btn-primary"><i class="icon icofont icofont-basket"></i><span class="body">Rs 254<small>.50</small></span></a>
                                        <span class="sdw"></span>
                                    </span>
                               </span>


                           </div>
                       </div>
                   </div>
                </div>

                <!-- Item -->
                <div class="item">
                   <div class="container">
                       <div class="row">

                           <div class="col-sm-12 col-md-5 hidden-xs hidden-sm">

                               <!-- Header -->
                               <h2 class="header text-uppercase text-blue">belt</h2>

                               <!-- Text -->
                               <p>
                                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et maxime vero amet, quisquam nihil! Odit, hic fugiat!
                               </p>

                               <!-- Buttons -->
                               <span class="btn-panel">

                                     <span class="sdw-wrap">
                                         <a href="shop-item.html" class="sdw-hover btn btn-lg btn-material btn-default"><span class="body">More info</span></a>
                                         <span class="sdw"></span>
                                     </span>

                                     <span class="hor-divider"></span>

                                     <span class="sdw-wrap">
                                         <a href="shop-item.html" class="sdw-hover btn btn-lg btn-material btn-primary"><i class="icon icofont icofont-basket"></i><span class="body">Buy now</span></a>
                                         <span class="sdw"></span>
                                     </span>
                               </span>
                           </div>

                           <div class="col-xs-10 col-xs-offset-1 col-md-7 col-md-offset-0">

                               <!-- Image -->
                               <div class="img">
                                   <img src="site-template/images/slideshow/img-02.png" alt="">
                               </div>

                               <!-- Badge -->
                               <span class="sale-badge bg-red text-uppercase">
                                   Hot!
                               </span>

                               <!-- Price -->
                               <span class="price hidden-xs">
                                   <span class="wrap text-red">
                                       Rs 122<small>.00</small>-
                                   </span>
                               </span>

                               <!-- Mobile button -->
                               <span class="text-center visible-xs">
                                    <span class="sdw-wrap">
                                        <a href="#" class="sdw-hover btn btn-lg btn-material btn-primary"><i class="icon icofont icofont-basket"></i><span class="body">Rs 122<small>.00</small></span></a>
                                        <span class="sdw"></span>
                                    </span>
                               </span>


                           </div>
                       </div>
                   </div>
                </div>

                <!-- Item -->
                <div class="item">
                   <div class="container">
                       <div class="row">

                           <div class="col-sm-12 col-md-5 hidden-xs hidden-sm">

                               <!-- Header -->
                               <h2 class="header text-uppercase text-blue">Camera</h2>

                               <!-- Text -->
                               <p>
                                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et maxime vero amet, quisquam nihil! Odit, hic fugiat!
                               </p>

                               <!-- Buttons -->
                               <span class="btn-panel">

                                     <span class="sdw-wrap">
                                         <a href="shop-item.html" class="sdw-hover btn btn-lg btn-material btn-default"><span class="body">More info</span></a>
                                         <span class="sdw"></span>
                                     </span>

                                     <span class="hor-divider"></span>

                                     <span class="sdw-wrap">
                                         <a href="shop-item.html" class="sdw-hover btn btn-lg btn-material btn-primary"><i class="icon icofont icofont-basket"></i><span class="body">Buy now</span></a>
                                         <span class="sdw"></span>
                                     </span>
                               </span>
                           </div>

                           <div class="col-xs-10 col-xs-offset-1 col-md-7 col-md-offset-0">

                               <!-- Image -->
                               <div class="img">
                                   <img src="site-template/images/slideshow/img-03.png" alt="">
                               </div>

                               <!-- Badge -->
                               <!--span class="sale-badge bg-green text-uppercase">
                                   new
                               </span-->

                               <!-- Price -->
                               <span class="price hidden-xs">
                                   <span class="wrap text-red">
                                       Rs 1 254<small>.50</small>-
                                   </span>
                               </span>

                               <!-- Mobile button -->
                               <span class="text-center visible-xs">
                                    <span class="sdw-wrap">
                                        <a href="#" class="sdw-hover btn btn-lg btn-material btn-primary"><i class="icon icofont icofont-basket"></i><span class="body">Rs 1 254<small>.50</small></span></a>
                                        <span class="sdw"></span>
                                    </span>
                               </span>


                           </div>
                       </div>
                   </div>
                </div>


            </div>
        </div>
    </div>
</div>
<!-- END: SLIDESHOW -->

<!--
SUBSTRATE
=============================================== -->
<div class="container-fluid">

    <div class="row">

        <div class="clearfix">

            <div class="substrate-wrap">

                <div class="substrate parallax-block"
                     data-speed-direction=".3"
                     data-default-pos="-600"
                     data-parallax-block="true">

                    <p class="text text-darkness">
                        ECOM
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: SUBSTRATE -->

<!--
BLOCK FEATURES
=============================================== -->
<div class="container block">

    <!-- Block header -->
    <div class="row">

        <div class="col-xs-12">

            <div class="block-header text-uppercase">

                <h2 class="header">Recent</h2>
            </div>
        </div>
    </div>
    <!-- /. Block header -->

    <div class="row">
        <div class="col-xs-12">
            <div class="owl-carousel owl-default features nav-top-left">
              @foreach ($latestProducts as $product)
              <div class="shop-item hover-sdw">
                  <div class="wrap">
                      <!-- Image & Caption -->
                      <div class="body">
                          <!-- Header -->
                          <div class="comp-header st-4 text-uppercase">
                            {{$product->name}}
                              <span>
                                {{ str_limit(implode(', ' , $product->categories->pluck('categoryname')->all()), $limit = 15, $end = '...') }}
                              </span>
                              <!-- Badge -->
                              <!--span class="sale-badge item-badge text-uppercase bg-green">
                                  New
                              </span-->
                          </div>
                          <!-- Image -->
                          <div class="image">
                            <img class="main" src="{{ $product->getFirstMediaUrl('productImages','thumb') }}" alt="">
                          </div>
                      </div>
                      <!-- Buy btn & more link -->
                      <div class="info">
                          <!-- Buy btn -->
                          <a href="shop-item.html" class="btn-material btn-price">
                              <!-- Price -->
                              <span class="price">
                                  <!-- Currency -->
                                  <span class="curr">
                                      Rs
                                  </span>
                                  <!-- Sale price
                                  <span class="sale">
                                      <span>234<small>.00</small></span>
                                  </span> -->
                                  <!-- Price -->
                                  <span class="price">
                                    {{$product->price}}
                                  </span>
                              </span>

                              <!-- Icon card -->
                              <span class="icon-card">
                                  <i class="icofont icofont-cart-alt"></i>
                              </span>
                          </a>


                          <!-- More link -->
                          <a href="{{'/product/'. $product->slug . '/' . $product->sku}}" class="more-link">More info</a>
                      </div>
                  </div>
              </div>
              @endforeach

            </div>
        </div>
    </div>
</div>
<!-- END: FEATURES -->

<!--
BLOCK BRAND INFO
=============================================== -->

<div class="bg-green container-fluid space-bottom-shift">

    <!-- Parallax wrapper -->
    <div class="container space-top space-bottom">

        <div class="row">

            <!-- Counter -->
            <div class="col-sm-3">

                <div class="counter">

                    <div class="wrap text-white">
                        25 789
                        <span>items sold</span>
                    </div>
                </div>
            </div>

            <!-- Counter -->
            <div class="col-sm-3">

                <div class="counter">

                    <div class="wrap text-white">
                        1 580
                        <span>happy customers</span>
                    </div>
                </div>
            </div>

            <!-- Counter -->
            <div class="col-sm-3">

                <div class="counter">

                    <div class="wrap text-white">
                        987
                        <span>items in store</span>
                    </div>
                </div>
            </div>

            <!-- Counter -->
            <div class="col-sm-3">

                <div class="counter">

                    <div class="wrap text-white">
                        12 580
                        <span>what else</span>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- END: BRAND INFO -->

<!--
BLOCK POPULAR ON SHOP
=============================================== -->
<div class="container-fluid block bg-grey-lightness space-top">
    <div class="row">

            <div class="container space-top">

                <div class="row hidden-xs">
                    <div class="col-xs-12 img-on-bg">
                        <img src="site-template/images/blocks/parallax-bg-popular-on-shop.png" alt="">
                    </div>
                </div>

                <div class="row">

                    <!-- Asside -->
                    <div class="col-md-4 col-lg-3 asside">

                      <!-- Block setup -->
                      <div class="inblock sdw">
                          <div class="wrap bg-white">
                            <form class="" action="/" method="get">

                                <!-- Header -->
                                <h3 class="header text-uppercase">Price</h3>

                                <!-- Price amount -->
                                <div class="price-slider"
                                     data-price-first=@if(request()->has('priceLow')){{explode(' ', request()->priceLow)[1]}} @else  0 @endif
                                     data-price-last=@if(request()->has('priceHigh')){{explode(' ', request()->priceHigh)[1]}} @else {{$maxPrice}} @endif
                                     data-price-max={{$maxPrice}}
                                     data-price-curr="Rs ">

                                    <div class="range"></div>

                                    <div class="amoutn">
                                        <input type="text" id='priceLow' name='priceLow' class="first" readonly>
                                        <input type="text" id='priceHigh' name='priceHigh' class="last" readonly>
                                    </div>
                                </div><!-- / Price amount -->
                                <!-- Divider -->
                                <div class="divider"></div><!-- / Divider -->
                                <button id="priceBtn" type="submit" class="btn btn-primary btn-material ripple-cont" >
                                  <div class="ripple-content">
                                            <span class="body">Apply</span>
                                  </div></button>

                            </form>
                          </div>
                      </div><!-- Block setup -->

                      <!-- Asside nav -->
                      <div class="asside-nav bg-white hidden-xs">
                          <div class="header text-uppercase text-white bg-blue">
                              Category
                          </div>

                          <ul class="nav-vrt bg-white">
                              @foreach($parentCategories as $parentCategory)
                              <li @if((request()->category == $parentCategory->id) || (in_array(request()->category, $parentCategory->children->pluck('id')->all()))) class='active' @endif>
                                  <a href="{{route('site-home',['category'=> $parentCategory->id])}}" class="btn-material">{{$parentCategory->categoryname}}
                                    @if($parentCategory->children->all() !=[])
                                      <i class="nav-icon-open icofont icofont-plus"></i>
                                      <i class="nav-icon-close icofont icofont-minus"></i>
                                  </a>
                                  <div class="sub-nav bg-grey-light">
                                      <ul class="sub">
                                          @foreach($parentCategory->children->all() as $childCategory)
                                            <li >
                                                <a href="{{route('site-home',['category'=> $childCategory->id])}}" class="btn-material">{{$childCategory->categoryname}}</a>
                                            </li>
                                          @endforeach
                                      </ul>
                                  </div>
                                  @else
                                    </a>
                                  @endif
                              </li>
                              @endforeach

                          </ul>

                      </div><!-- / Asside nav -->


                        <!-- List categories for mobile -->
                        <div class="inblock padding-none visible-xs">
                            <div class="mobile-category nav-close">

                                <!-- Header -->
                                <div class="header bg-blue">
                                    <span class="head">Category</span>

                                    <span class="btn-swither" >
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </span>
                                </div>

                                <ul class="nav-vrt bg-white">
                                    <li class="active">
                                        <a href="#" class="btn-material">Man line
                                            <i class="nav-icon-open icofont icofont-plus"></i>
                                            <i class="nav-icon-close icofont icofont-minus"></i>
                                        </a>

                                        <div class="sub-nav bg-grey-light">
                                            <ul class="sub">
                                                <li>
                                                    <a href="#" class="btn-material">Shirts</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Pants</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Footwear</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Belts</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Bags</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Accessories</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Perfume</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Jewerly</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a href="#" class="btn-material">Woman
                                            <i class="nav-icon-open icofont icofont-plus"></i>
                                            <i class="nav-icon-close icofont icofont-minus"></i>
                                        </a>

                                        <div class="sub-nav bg-grey-light">
                                            <ul class="sub">
                                                <li>
                                                    <a href="#" class="btn-material">Shirts</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Pants</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Footwear</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Belts</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Bags</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Accessories</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Perfume</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-material">Jewerly</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a href="#" class="btn-material">Jewerly</a>
                                    </li>

                                    <li>
                                        <a href="#" class="btn-material">Electronics</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div><!-- ./ Asside -->

                    <!-- Item list -->
                    <div class="col-md-8 col-lg-9 shop-items-set shop-items-full">

                      <!-- Paginations -->
                      <div class="row pagination-block hidden-xs">
                          <div class="col-xs-12">

                              <div class="wrap">
                                  <!-- Pagination -->
                                  <ul class="pagination">
                                      <li>
                                          <a href="{{route('site-home', [ 'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category])}}">
                                              <span><i class="icofont icofont-rounded-left"></i></span>
                                          </a>
                                      </li>
                                      @for ($i = 1; $i <= ceil($products->total()/$count); $i++)
                                          <li @if($i==request()->page || (request()->page == null && $i==1)) class='active' @endif><a href="{{route('site-home',['page'=>$i, 'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category ])}}">{{$i}}</a></li>
                                      @endfor

                                      <li>
                                          <a href="{{route('site-home',['page'=>ceil($products->total()/$count),  'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category])}}">
                                              <span><i class="icofont icofont-rounded-right"></i></span>
                                          </a>
                                      </li>
                                  </ul>

                              </div>

                          </div>
                      </div>
                        <!-- Item list -->
                        <div class="row item-wrapper">
                            @if($products->all()==[])
                              <div class="col-xs-6">
                                <p>No Products Found</p>
                              </div>
                            @endif
                            @foreach ($products as $product)
                            <!-- Shop item  -->
                            <div class="col-xs-6 col-sm-4 col-md-6 col-lg-4 shop-item hover-sdw timer"
                                 data-timer-date="2018, 2, 5, 0, 0, 0">

                                <div class="wrap">

                                    <!-- Image & Caption -->
                                    <div class="body">

                                        <!-- Header -->
                                        <div class="comp-header st-4 text-uppercase">

                                            {{$product->name}}
                                            <span>
                                                {{ str_limit(implode(', ' , $product->categories->pluck('categoryname')->all()), $limit = 15, $end = '...') }}
                                            </span>

                                        </div>

                                        <!-- Image -->
                                        <div class="image">
                                            <img class="main" src="{{ $product->getFirstMediaUrl('productImages','thumb') }}" alt="">
                                        </div>

                                        <!-- Caption -->
                                        <div class="caption">

                                            <!-- Features list -->
                                            <ul class="features">
                                                <li>
                                                    <i class="icofont icofont-shield"></i>
                                                    <span>24 days. Money Back Guarantee</span>
                                                </li>
                                                <li>
                                                    <i class="icofont icofont-ship"></i>
                                                    <span>Free shipping</span>
                                                </li>
                                                <li>
                                                    <i class="icofont icofont-hand"></i>
                                                    <span>Free help and setup</span>
                                                </li>
                                            </ul>

                                            <!-- Text -->
                                            <p class="text">
                                                Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                                            </p>
                                        </div>
                                    </div>

                                    <!-- Buy btn & more link -->
                                    <div class="info">

                                        <!-- Buy btn -->
                                        <a href="#" class="btn-material btn-price">

                                            <!-- Price -->
                                            <span class="price">

                                                <!-- Currency -->
                                                <span class="curr">
                                                    Rs
                                                </span>


                                                <!-- Price -->
                                                <span class="price">
                                                    {{$product->price}}
                                                </span>
                                            </span>

                                            <!-- Quantity -->
                                            <span class="qnt-select">
                                                <span class="plus">
                                                    <i class="icofont icofont-plus"></i>
                                                </span>
                                                <span class="view-sum">
                                                    01
                                                </span>
                                                <span class="minus">
                                                    <i class="icofont icofont-minus"></i>
                                                </span>
                                            </span>

                                            <!-- Icon card -->
                                            <span class="icon-card">
                                                <i class="icofont icofont-cart-alt"></i>
                                            </span>
                                        </a>


                                        <!-- More link -->
                                        <a href="{{'/product/'. $product->slug . '/' . $product->sku}}" class="more-link">More info</a>
                                    </div>
                                </div>
                            </div>
                            <!-- / Shop item -->

                            @endforeach

                        </div>

                        <!-- Paginations -->
                        <div class="row pagination-block bottom">
                            <div class="col-xs-12">

                              <div class="wrap">
                                  <!-- Pagination -->
                                  <ul class="pagination">
                                      <li>
                                          <a href="{{route('site-home', [ 'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category])}}">
                                              <span><i class="icofont icofont-rounded-left"></i></span>
                                          </a>
                                      </li>
                                      @for ($i = 1; $i <= ceil($products->total()/$count); $i++)
                                          <li @if($i==request()->page || (request()->page == null && $i==1)) class='active' @endif><a href="{{route('site-home',['page'=>$i, 'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category ])}}">{{$i}}</a></li>
                                      @endfor

                                      <li>
                                          <a href="{{route('site-home',['page'=>ceil($products->total()/$count),  'priceLow'=>request()->priceLow, 'priceHigh'=>request()->priceHigh, 'category'=>request()->category])}}">
                                              <span><i class="icofont icofont-rounded-right"></i></span>
                                          </a>
                                      </li>
                                  </ul>

                              </div>
                            </div>
                        </div>

                    </div><!-- ./ Item list -->
                </div>
            </div>

    </div><!-- / Parallax wrapper -->
</div>
<!-- END: POPULAR ON SHOP -->



@endsection
