@extends('layouts.site')

@section('title', 'Contact')

@section('content')
<div class="container">
    <div class="space-top space-bottom">
      <div class="row">
        <div class="col-lg-8 col-12">
          <div class="sdw-block">
              <div class="wrap bg-white">
                <div class="row auth-form" style="padding-bottom: 0px">
                  <div class="col-md-12">
                    <!-- Header -->
                    <h1 class="header text-uppercase">
                                            Get In Touch
                                            <span>
                                                WIth us
                                            </span>
                    </h1>
                  </div>
                </div>

              <form class="form" method="post" action="/contact">
                @csrf
                <div class="row">

                  <div class="col-lg-6 col-12">

                    <div class="form-group mb-3">
                      <label for="name">Your Name</label>
                      <input placeholder="Name" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                      <div class="input-group-append">
                        <div class="input-group-text">
                        </div>
                      </div>
                      @error('name')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-lg-6 col-12">
                    <div class="form-group mb-3">
                      <label for="subject">Your Subject</label>
                      <input placeholder="Subject" id="subject" type="text" class="form-control @error('subject') is-invalid @enderror" name="subject" value="{{ old('subject') }}" required autocomplete="subject" autofocus>
                      <div class="input-group-append">
                        <div class="input-group-text">
                        </div>
                      </div>
                      @error('subject')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6 col-12">

                    <div class="form-group mb-3">
                      <label for="email">Your Email</label>
                      <input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                      <div class="input-group-append">
                        <div class="input-group-text">
                        </div>
                      </div>
                      @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-lg-6 col-12">

                    <div class="form-group mb-3">
                      <label for="phone">Your Phone</label>
                      <input placeholder="phone" id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>
                      <div class="input-group-append">
                        <div class="input-group-text">
                        </div>
                      </div>
                      @error('phone')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col col-xs-12">
                    <div class="form-group mb-4">
                      <label>Your Message<span>*</span></label>
                      <textarea class="form-control" name="message" placeholder=""></textarea>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12" style="margin-top:  10px">
                    <span class="sdw-wrap">
                        <button type="submit" class="sdw-hover btn btn-material btn-yellow ripple-cont">Send Message</button>
                        <span class="sdw"></span>
                    </span>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="sdw-block">
              <div class="wrap bg-white">
                <div class="single-info">
                  <i class="fas fa-phone"></i>
                  <h4 class="title">Call us Now:</h4>
                  <ul>
                    <li>{{env('CONTACT_1')}}</li>
                    <li>{{env('CONTACT_2')}}</li>
                  </ul>
                </div>
                <div class="single-info">
                  <i class="fa fa-envelope-open"></i>
                  <h4 class="title">Email:</h4>
                  <ul>
                    <li><a href="{{'mailto:'.env('EMAIL_1')}}">{{env('EMAIL_1')}}</a></li>
                    <li><a href="{{'mailto:'.env('EMAIL_2')}}">{{env('EMAIL_2')}}</a></li>
                  </ul>
                </div>
                <div class="single-info">
                  <i class="fa fa-location-arrow"></i>
                  <h4 class="title">Our Address:</h4>
                  <ul>
                    <li>{{env('ADDRESS')}}</li>
                  </ul>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
