@extends('layouts.site')
@section('content')
<div class="container space-top space-bottom">



  <div class="row auth-form">
      <div class="col-md-3">
        <div class="row auth-form" style="padding-bottom: 0px">
                        <div class="col-md-12">
                          <!-- Header -->
                          <h1 class="header text-uppercase" style="font-size: 3.5rem">
                                                  View
                                                  <span>
                                                      User Profile
                                                  </span>
                          </h1>
                        </div>
        </div>
          <!-- Nav -->
          <div class="asside-nav no-bg">
              <ul class="nav-vrt border">
                  <li class="active">
                      <a href="#" class="tablinks btn-material" onclick="openTab(event, 'dashboard')">Dashboard</a>
                  </li>
                  <li>
                      <a href="#" class="tablinks btn-material" onclick="openTab(event, 'orders')">My Orders</a>
                  </li>
                  <li>
                      <a href="#" class="tablinks btn-material" onclick="openTab(event, 'profile')">Edit Account Details</a>
                  </li>
                  <li>
                      <a href="#" class="tablinks btn-material" onclick="openTab(event, 'address')">Alter Address</a>
                  </li>
                  <li>
                      <a href="#" class="tablinks btn-material" onclick="openTab(event, 'password-change')">Change Password</a>
                  </li>

              </ul>
          </div>
      </div>
      <div class="col-md-8 col-md-offset-1 form-fields">
        <div id="dashboard" class="tabcontent">
          <div class="row">
            <div class="col col-md-4">
              <div class="sdw-block">
                <div class="wrap bg-white">

                    <!-- Caption -->
                    <div class="caption">

                        <!-- Header -->
                        <span class="header text-uppercase" style="font-size:4rem">
                            150
                        </span>
                        <p>Orders Placed</p>
                        <!-- Text -->


                    </div>
                </div>
              </div>
            </div>
            <div class="col col-md-4">
              <div class="sdw-block">
                <div class="wrap bg-white">

                    <!-- Caption -->
                    <div class="caption">

                        <!-- Header -->
                        <span class="header text-uppercase" style="font-size:4rem">
                            1500
                        </span>
                        <p>Items Bought</p>
                        <!-- Text -->


                    </div>
                </div>
              </div>
            </div>
            <div class="col col-md-4">
              <div class="sdw-block">
                <div class="wrap bg-white">

                    <!-- Caption -->
                    <div class="caption">

                        <!-- Header -->
                        <span class="header text-uppercase" style="font-size:4rem">
                            30
                        </span>
                        <p>Orders Returned</p>
                        <!-- Text -->


                    </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div id="orders" class="tabcontent">

          <div class="my-list no-shadow" style="box-shadow: 0px 0px">
              <div class="wrap bg-white" >
                  <!-- Header -->
                  <div class="list-header text-uppercase bg-green text-white">
                      <div class="order-id">
                          Order ID
                      </div>
                      <div class="date">
                          Date
                      </div>
                      <div class="status">
                          Status
                      </div>
                      <div class="total1">
                          Total
                      </div>
                      <div class="action">

                      </div>
                  </div><!-- / Header -->
                  <!-- List body -->
                  <div class="list-body">
                      <!-- Item -->
                      <div class="item">
                          <!-- Checkbox -->
                          <div class="order-id">
                              <p>#1231</p>
                          </div>

                          <div class="date">
                              <p>12-Jan-2021</p>
                          </div>

                          <div class="status">
                              <p>Delivered</p>
                          </div>

                          <div class="total1">
                              <i class="icofont icofont-rupee">Rs</i>
                              <span>520.20</span>
                          </div>

                          <div class="action">
                              <button type="button" class="btn-material btn-yellow btn" name="button" >Details</button>
                          </div>

                      </div>
                      <div class="item">
                          <!-- Checkbox -->
                          <div class="order-id">
                              <p>#1231</p>
                          </div>
                          <div class="date">
                              <p>12-Jan-2021</p>
                          </div>
                          <div class="status">
                              <p>Delivered</p>
                          </div>

                          <div class="total1">
                              <i class="icofont icofont-rupee">Rs</i>
                              <span>520.20</span>
                          </div>

                          <div class="action">
                              <button type="button" class="btn-material btn-yellow btn" name="button">Details</button>
                          </div>

                      </div>
                  </div><!-- / List body -->

                  <!-- Footer -->

              </div>
          </div>
        </div>
        <div id="profile" class="tabcontent">
          <form method="POST" action="">
              @csrf
                <div class="form-group pd-none">
                    <label for="name">Name</label>
                    <div class="">
                      <input id="name" type="text" value="{{$user->name}}" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                      @error('name')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                    </div>
                </div>
                <div class="form-group pd-none">
                    <label for="email">Email</label>
                    <div class="">
                      <input id="email" type="email" value="{{$user->email}}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                      @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                    </div>
                </div>
                <div class="form-group pd-none">
                    <label for="phone" class="">Phone number</label>
                    <div class="">
                      <input id="phone" value="{{$user->phone}}" type="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" required autocomplete="new-phone">

                      @error('phone')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                    </div>
                </div>
                <div class="">
                  <button type="submit" class="btn btn-primary btn-lg">
                      Edit
                  </button>
                </div>

            </form>
        </div>
        <div id="address" class="tabcontent">
          <form class="form-horizontal">

              <!-- Authocompille -->
              <div class="form-group pd-none">
                  <label for="autocomplete" class="">Address</label>
                  <div class="">
                      <input type="text" value="{{$user->addresses->all()[0]->address}}"
                             class="form-control"
                             id="autocomplete"
                             onFocus="geolocate()">
                  </div>
              </div>


              <div class="form-group pd-none">
                  <label for="locality" >City</label>
                  <div class="">
                      <input type="text" value="{{$user->addresses->all()[0]->city}}"
                             class="form-control"
                             id="locality"
                             >
                  </div>
              </div>

              <div class="form-group pd-none">
                  <label for="administrative_area_level_1">State</label>
                  <div class="">
                      <input type="text" value="{{$user->addresses->all()[0]->state}}"
                             class="form-control"
                             id="administrative_area_level_1"
                             >
                  </div>
              </div>

              <div class="form-group pd-none">
                  <label for="postal_code">Zip code</label>
                  <div class="">
                      <input type="text" value="{{$user->addresses->all()[0]->zip}}"
                             class="form-control"
                             id="postal_code"
                             >
                  </div>
              </div>

              <div class="form-group pd-none">
                  <label for="country" >Country</label>
                  <div class="">
                      <input type="text"
                             class="form-control"  value="{{$user->addresses->all()[0]->country}}"
                             id="country"
                             >
                  </div>
              </div>
              <!-- / Authocompille -->


              <div class="form-group" style="margin-top: 10px">
                  <div class="col-sm-offset-3 col-sm-8">
                      <span class="sdw-wrap">
                          <a href="/checkout" class="sdw-hover btn btn-material btn-yellow btn-lg ripple-cont">Replace</a>
                          <span class="sdw"></span>
                      </span>
                  </div>
              </div>

          </form>
        </div>
        <div id="password-change" class="tabcontent">
          <form method="POST" action="">
              @csrf
              <div class="form-group pd-none">
                  <label for="old-password" class="">Enter old password</label>
                  <div class="">
                    <input id="old-password" type="password" class="form-control @error('old-password') is-invalid @enderror" name="old-password" required>
                    @error('old-password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
              </div>
              <div class="form-group pd-none">
                  <label for="password" class="">Enter new password</label>
                  <div class="">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
              </div>
              <div class="form-group pd-none">
                  <label for="password-confirm" class="">Confirm password</label>
                  <div class="">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                  </div>
              </div>
              <div class="">
                <button type="submit" class="btn btn-primary btn-lg">
                    Confirm Change
                </button>
              </div>
          </form>
        </div>


      </div>
  </div>
</div>

<script type="text/javascript">

  function openTab(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace("active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }

</script>
@endsection
