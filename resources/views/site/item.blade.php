@extends('layouts.site')
@section('breadcrumb')
        <!--
        BREADCRUMBS
        =============================================== -->
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <ol class="breadcrumb bg-green">
                        <li><a href="/">Homepage</a></li>
                        <li><a href="/shop">Products</a></li>
                        <li class="active">{{$product->name}}</li>
                    </ol>

                </div>
            </div>
        </div>
        <!-- END: BREADCRUMBS -->
@endsection

@section('content')
        <div class="container-fluid ">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-lg-9">

                            <!--
                            MAIN INFO
                            =============================================== -->
                            <div class="row shop-item-page">

                                <!-- ITEM GALLERY BLOCK -->
                                <div class="col-sm-4 col-md-5 fix-height">
                                    <div class="item-gallery float-block">

                                        <div class="owl-carousel image">

                                            <div class="item">
                                                <img src="{{ $product->getFirstMediaUrl('productImages','thumb') }}" alt="">
                                            </div>

                                            <div class="item">
                                                <img src="/site-template/images/shop/item-page/img-02.jpg" alt="">
                                            </div>

                                            <div class="item">
                                                <img src="/site-template/images/shop/item-page/img-03.jpg" alt="">
                                            </div>

                                        </div>

                                        <div class="owl-carousel image-nav hidden-xs">
                                            <div class="item">
                                                <img src="{{ $product->getFirstMediaUrl('productImages','thumb') }}" alt="">
                                            </div>

                                            <div class="item">
                                                <img src="/site-template/images/shop/item-page/img-02.jpg" alt="">
                                            </div>

                                            <div class="item">
                                                <img src="/site-template/images/shop/item-page/img-03.jpg" alt="">
                                            </div>
                                        </div>

                                    </div>
                                </div><!-- / ITEM GALLERY BLOCK -->

                                <!-- CAPTION BLOCK -->
                                <div class="col-sm-8 col-md-7 get-height">

                                    <!-- Item header -->
                                    <div class="row item-header">

                                        <div class="col-md-7">

                                            <h1 class="comp-header st-12 text-uppercase text-blue">
                                                {{$product->name}}

                                                <span class="text-dark">
                                                  {{implode(', ' , $product->categories->pluck('categoryname')->all())}}
                                                </span>

                                            </h1>
                                        </div>


                                    </div>

                                    <!-- Divider -->
                                    <div class="divider-dotted"></div>

                                    <!-- Price & rating panel -->
                                    <div class="row price-pan">

                                        <!-- Price & rating -->
                                        <div class="col-md-12">

                                            <span class="head">Price</span>

                                            <span class="price">
                                                <!-- Price -->
                                                <span class="price">

                                                    <!-- Currency -->
                                                    <span class="curr">Rs </span>
                                                    {{$product->price}}
                                                </span>
                                            </span>


                                        </div>


                                    </div>

                                    <!-- Divider -->
                                    <div class="divider-dotted"></div>



                                    <!-- Buy btn panel -->
                                    <div class="row">
                                        <div class="col-xs-12">

                                            <div class="buy-btn-panel bg-green">

                                                <!-- Cart icon -->
                                                <div class="cart-icon">
                                                    <i class="icofont icofont-basket"></i>
                                                </div>

                                                <!-- Btns -->
                                                <div class="btns-wrap btn-material bg-white">
                                                    <a href="#">Buy now</a>
                                                    <a class="text-blue" href="#">Put in cart</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <!-- Description -->
                                    <div class="row description">
                                        <div class="col-xs-12">
                                            <h2 class="header">
                                                Description:
                                            </h2>

                                            <p>
                                                {{$product->description}}
                                            </p>

                                        </div>
                                    </div>

                                    <!-- Features panel -->
                                    <div class="row features-pan hidden-xs">
                                        <div class="col-xs-12">

                                            <ul class="row features-list">
                                                <li class="col-md-4">
                                                    <i class="icofont icofont-shield"></i>
                                                    <span>24 days. Money Back Guarantee</span>
                                                </li>
                                                <li class="col-md-4">
                                                    <i class="icofont icofont-ship"></i>
                                                    <span>Free shipping</span>
                                                </li>
                                                <li class="col-md-4">
                                                    <i class="icofont icofont-hand"></i>
                                                    <span>Free help and setup</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div><!-- / CAPTION BLOCK -->

                            </div>
                            <!-- END: MAIN INFO -->

                        </div>

                        <!-- Asside -->
                        <div class="visible-lg col-md-4 col-lg-3 asside">


                            <!-- Asside nav -->
                            <div class="asside-nav bg-white hidden-xs">
                                <div class="header text-uppercase text-white bg-blue">
                                    Category
                                </div>

                                <ul class="nav-vrt bg-white">
                                    @foreach($parentCategories as $parentCategory)
                                    <li>
                                        <a href="#" class="btn-material">{{$parentCategory->categoryname}}
                                            <i class="nav-icon-open icofont icofont-plus"></i>
                                            <i class="nav-icon-close icofont icofont-minus"></i>
                                        </a>

                                        <div class="sub-nav bg-grey-light">
                                            <ul class="sub">
                                                @foreach($parentCategory->children->all() as $childCategory)
                                                  <li>
                                                      <a href="#" class="btn-material">{{$childCategory->categoryname}}</a>
                                                  </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                    @endforeach

                                </ul>

                            </div><!-- / Asside nav -->


                        </div><!-- ./ Asside -->

                    </div>
                </div>
            </div>
        </div>

@endsection
