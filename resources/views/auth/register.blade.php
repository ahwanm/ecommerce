@extends('layouts.site')

@section('breadcrumb')
<!--
        BREADCRUMBS
        =============================================== -->
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <ol class="breadcrumb bg-green">
                        <li><a href="/">Homepage</a></li>
                        <li class="active">Register</li>
                    </ol>

                </div>
            </div>
        </div>
        <!-- END: BREADCRUMBS -->
@endsection
@section('content')
        <div class="container">

            <!--
            CONTENT
            =============================================== -->
            <div class="row block none-padding-top">

                <div class="col-xs-12">
                    <div class="sdw-block">
                        <div class="wrap bg-white">

                            <!-- Authirize form -->
                            <div class="row head-block">

                                <!-- Header & nav -->
                                <div class="col-md-12">

                                    <!-- Header -->
                                    <h1 class="header text-uppercase">
                                        New user
                                        <span>
                                            registration
                                        </span>
                                    </h1>
                                </div>
                            </div>
                            <!-- Authirize form -->

                            <div class="row space-bottom">

                                <!-- Header & nav -->

                                <div class="col-md-12">
                                  <form method="POST" action="{{ route('register') }}">
                                      @csrf
                                        <div class="form-group pd-none">
                                            <label for="name" class="col-sm-3 control-label text-darkness">Your Name</label>
                                            <div class="col-sm-8">
                                              <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                              @error('name')
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                              @enderror
                                            </div>
                                        </div>
                                        <div class="form-group pd-none">
                                            <label for="email" class="col-sm-3 control-label text-darkness">Enter your email</label>
                                            <div class="col-sm-8">
                                              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                              @error('email')
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                              @enderror
                                            </div>
                                        </div>
                                        <div class="form-group pd-none">
                                            <label for="password" class="col-sm-3 control-label text-darkness">Enter your password</label>
                                            <div class="col-sm-8">
                                              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                              @error('password')
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                              @enderror
                                            </div>
                                        </div>
                                        <div class="form-group pd-none">
                                            <label for="password-confirm" class="col-sm-3 control-label text-darkness">Confirm password</label>
                                            <div class="col-sm-8">
                                              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                            </div>
                                        </div>
                                        <div class="form-group pd-none">
                                            <label for="phone" class="col-sm-3 control-label text-darkness">Enter your phone number</label>
                                            <div class="col-sm-8">
                                              <input id="phone" type="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" required autocomplete="new-phone">

                                              @error('phone')
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                              @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-offset-3 col-sm-7">
                                          <button type="submit" class="btn btn-primary">
                                              {{ __('Register') }}
                                          </button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END: CONTENT -->

          </div>
@endsection
