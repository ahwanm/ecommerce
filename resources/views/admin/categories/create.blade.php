@extends('layouts.admin')
@section('title', 'Create Category')
@section('header-content')
  <!-- Select2 -->
  <link rel="stylesheet" href="/admin-lte/plugins/select2/css/select2.min.css">
@endsection
@section('content-header')

<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Add Category</h1>
    </div>
  </div>
</div><!-- /.container-fluid -->


@endsection

@section('content')
<div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Enter Category Details</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="quickForm" method="post" action="/admin/categories" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col form-group">
                    <label for="name">Category Name</label>
                    <input type="text" value="{{old('name')}}" name="name" class="form-control" id="name" placeholder="Enter Category Name">
                    @error('name')
                      <p class="text-danger">{{$errors->first('name')}}</p>
                    @enderror
                  </div>

                </div>
                <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" name="categoryDescription" rows="3" placeholder="Enter category description">{{old('categoryDescription')}}</textarea>
                  @error('categoryDescription')
                    <p class="text-danger">{{$errors->first('categoryDescription')}}</p>
                  @enderror
                </div>
                <div class="row">
                  <div class="col col-8 form-group">
                        <label for="categoryImage">Category Image</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="file" class="form-control" name="categoryImage" id="categoryImage" required>
                            <label class="custom-file-label" for="categoryImage">Choose Category Image</label>
                          </div>
                        </div>
                        @error('categoryImage')
                          <p class="text-danger">{{$errors->first('categoryImage')}}</p>
                        @enderror
                  </div>
                  <div class="col col-4">
                    <label for="categoryImage">Parent Category</label>
                    <select class="form-control select2 select2-hidden-accessible" name="parentCategory" id="parentCategory" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                      <option value="">Root</option>
                      @foreach ($categories->whereNull('parent_id') as $category)
                        <option @if(old('parentCategory') == $category->id){{"selected"}} @endif value="{{ $category->id }}">{{ $category->categoryname }}</option>
                      @endforeach
                    </select>
                    <!-- <select class="select2" name="categories[]" id="categories" multiple="multiple" data-placeholder="Select categories" style="width: 100%;">

                    </select> -->
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          </div>



            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">

        </div>
        <!--/.col (right) -->
      </div>
@endsection

@section('script-content')
  <script>
    $('#categoryImage').on('change',function(){
              var fileName = $(this).val().split("\\").pop();
              //replace the "Choose a file" label
              $(this).next('.custom-file-label').html(fileName);
    })
  </script>
  <!-- Select2 -->
  <script src="/admin-lte/plugins/select2/js/select2.full.min.js"></script>
  <script>
    $('.select2').select2();
    $('#productImage').on('change',function(){
              var fileName = $(this).val().split("\\").pop();
              //replace the "Choose a file" label
              $(this).next('.custom-file-label').html(fileName);
    })
  </script>

@endsection
