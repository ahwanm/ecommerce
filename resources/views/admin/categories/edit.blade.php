@extends('layouts.admin')
@section('title', 'Edit Categories')
@section('header-content')
  <!-- Select2 -->
  <link rel="stylesheet" href="/admin-lte/plugins/select2/css/select2.min.css">
@endsection
@section('content-header')

<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Edit Category</h1>
    </div>
  </div>
</div><!-- /.container-fluid -->


@endsection

@section('content')
<div class="row">
        <!-- left column -->
        <div class="col-md-9">
          <!-- jquery validation -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Alter Category Details</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="quickForm" method="post" action="/admin/categories/{{$category->id}}">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col form-group">
                    <label for="slug">Slug</label>
                    <input readonly value="{{ $errors->any() && old('slug')!=null ? old('slug'):$category->slug}}" type="text" name="slug" class="form-control" id="slug" placeholder="slug">
                  </div>
                  <div class="col form-group">
                    <label for="name">Category Name</label>
                    <input type="text" value="{{ $errors->any() && old('name')!=null ? old('name'):$category->categoryname}}" name="name" class="form-control" id="name" placeholder="Enter Category Name">
                    @error('name')
                      <p class="text-danger">{{$errors->first('name')}}</p>
                    @enderror
                  </div>
                  <div class="col form-group">
                    <label for="name">Parent Category</label>
                    <select class="form-control select2 select2-hidden-accessible" name="parentCategory" id="parentCategory" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                      <option value="">Root</option>
                      @foreach ($categories->whereNull('parent_id') as $cat)
                        @if($cat->id != $category->id)
                          <option  @if( $cat->id == $category->parent_id ){{"selected"}} @endif value="{{ $cat->id }}">{{ $cat->categoryname }}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" name="categoryDescription" rows="3" placeholder="Enter category description">{{ $errors->any() && old('categoryDescription')!=null ? old('categoryDescription'):$category->description}}</textarea>
                  @error('categoryDescription')
                    <p class="text-danger">{{$errors->first('categoryDescription')}}</p>
                  @enderror
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-3">
          <form id="quickForm" method="POST" action="/admin/categories/{{$category->id}}/imageReplace" enctype="multipart/form-data">
            @csrf
            <div class="card card-default">
              <div class="card-header bg-primary">
                <h3 class="card-title text-light">Category Image</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body" style="display: block;">
                <div class="image-upload">
                  <label for="file-input">
                    <img  width="100%" src="{{ $category->getFirstMediaUrl('categoryImages') }}" alt="" loading="lazy" srcset="" sizes="(max-width: 266px) 100vw, 266px">
                  </label>
                  <input id="categoryImage" name="categoryImage" type="file" />
                  @error('categoryImage')
                    <p class="text-danger">{{$errors->first('categoryImage')}}</p>
                  @enderror
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer" style="display: block;">
                <button type="submit" class="btn btn-primary">Replace Image</button>

              </div>
            </div>
          </form>

        </div>
        <!--/.col (right) -->
      </div>
@endsection
@section('script-content')
  <!-- Select2 -->
  <script src="/admin-lte/plugins/select2/js/select2.full.min.js"></script>
  <script>
    $('.select2').select2();
    $('#productImage').on('change',function(){
              var fileName = $(this).val().split("\\").pop();
              //replace the "Choose a file" label
              $(this).next('.custom-file-label').html(fileName);
    })
  </script>

@endsection
