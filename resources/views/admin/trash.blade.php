@extends('layouts.admin')
@section('title', 'Trash')
@section('header-content')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
@endsection
@section('content-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Trash</h1>
      <p class="text-primary">{{$msg}}</p>
    </div>
  </div>
</div><!-- /.container-fluid -->
@endsection
@section('content')
<div class="card">
              <!-- /.card-header -->
  <div class="card-body">

    <div class="dataTables_wrapper dt-bootstrap4">

      <div class="row">
        <div class="col-sm-12">
          <ul class="nav nav-tabs mb-3" id="custom-content-above-tab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="user-content-tab" data-toggle="pill" href="#user-content" role="tab" aria-controls="user-content" aria-selected="true">Users</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="product-content-tab" data-toggle="pill" href="#product-content" role="tab" aria-controls="product-content" aria-selected="false">Products</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="category-content-tab" data-toggle="pill" href="#category-content" role="tab" aria-controls="category-content" aria-selected="false">Categories</a>
              </li>
          </ul>
          <div class="tab-content" id="custom-content-above-tabContent">
              <div class="tab-pane fade active show" id="user-content" role="tabpanel" aria-labelledby="user-content-tab">
                <table id="" class="table table-bordered table-condensed data-table" role="grid" aria-describedby="example2_info">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Type</th>
                            <th width="150px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
              </div>
              <div class="tab-pane fade" id="product-content" role="tabpanel" aria-labelledby="product-content-tab">
                <table id="" class="table table-bordered table-condensed data-table1" role="grid" aria-describedby="example2_info">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Sku</th>
                          <th>Image</th>
                          <th>Name</th>
                          <th>Quantity</th>
                          <th>Price</th>
                          <th>Creator</th>
                          <th width="150px">Action</th>
                      </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
              <div class="tab-pane fade" id="category-content" role="tabpanel" aria-labelledby="category-content-tab">
                <div class="row">
                  <div class="col-sm-12">
                    <table id="" class="table table-bordered table-condensed data-table2" role="grid" aria-describedby="example2_info">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th width="150px">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>

        </div>
      </div>
    </div>
  </div>
              <!-- /.card-body -->
</div>
@endsection
@section('script-content')
<script type="text/javascript">
  $(function () {
    var table = $('.data-table').DataTable({
        processing: true,
        autoWidth: false,
        serverSide: true,
        ajax: "{{ route('admin-trash-user') }}",
        order: [[ 1, 'asc' ]],
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'type', name: 'type'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  });
  $(function () {
    var table = $('.data-table1').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        order: [[ 3, 'asc' ]],
        ajax: "{{ route('admin-trash-product') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'sku', name: 'sku'},
            {data: 'image', name: 'image', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'quantity', name: 'quantity'},
            {data: 'price', name: 'price'},
            {data: 'creator', name: 'creator'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  });
  $(function () {
    var table = $('.data-table2').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ajax: "{{ route('admin-trash-category') }}",
        order: [[ 2, 'asc' ]],
        responsive: true,
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'image', name: 'image', orderable: false, searchable: false},
            {data: 'categoryname', name: 'categoryname'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  });
  var url = document.location.toString();
  if (url.match('#')) {
      $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
  }

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      window.location.hash = e.target.hash;
  });
</script>
@endsection
