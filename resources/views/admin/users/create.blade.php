@extends('layouts.admin')
@section('title', 'Create User')

@section('content-header')

<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Add User</h1>
    </div>
  </div>
</div><!-- /.container-fluid -->


@endsection

@section('content')
<div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Enter Details</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="quickForm" method="post" action="/admin/users">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col col-12 form-group">
                    <label for="name">Name</label>
                    <input type="text" value="{{old('name')}}" name="name" class="form-control" id="name" placeholder="Enter Name">
                    @error('name')
                      <p class="text-danger">{{$errors->first('name')}}</p>
                    @enderror
                  </div>
                  <div class="col col-12 form-group">
                    <label for="email">Email Address</label>
                    <input type="email" value="{{old('email')}}" name="email" class="form-control" id="email" placeholder="Enter email">
                    @error('email')
                      <p class="text-danger">{{$errors->first('email')}}</p>
                    @enderror
                  </div>
                </div>
                <div class="row">
                  <div class="col col-md-4 col-12 form-group">
                    <label for="password">Password</label>
                    <input type="password" value="{{old('password')}}" name="password" class="form-control" id="password" placeholder="Enter Password">
                    @error('password')
                      <p class="text-danger">{{$errors->first('password')}}</p>
                    @enderror
                  </div>
                  <div class="col col-md-4 col-12 form-group">
                    <label for="phone">Phone Number</label>
                    <input type="text" value="{{old('phone')}}" name="phone" class="form-control" id="phone" placeholder="Enter Phone Number">
                    @error('phone')
                      <p class="text-danger">{{$errors->first('phone')}}</p>
                    @enderror
                  </div>
                  <div class="col col-md-4 col-12 form-group">
                        <label>Type</label>
                        <select name="type" class="custom-select">
                          <option {{ old('type') == "admin" ? "selected" : "" }} value='admin'>Admin</option>
                          <option {{ old('type') == "customer"? "selected" : "" }} value='customer'>Customer</option>
                        </select>
                        @error('type')
                          <p class="text-danger">{{$errors->first('type')}}</p>
                        @enderror
                  </div>
                </div>
                <div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" name="address" rows="3" placeholder="Enter Address">{{old('address')}}</textarea>
                  @error('address')
                    <p class="text-danger">{{$errors->first('address')}}</p>
                  @enderror
                </div>
                <div class="row">
                  <div class="col col-md-3 col-12 form-group">
                    <label for="city">City</label>
                    <input type="text" value="{{old('city')}}" name="city" class="form-control" id="city" placeholder="Enter City">
                    @error('city')
                      <p class="text-danger">{{$errors->first('city')}}</p>
                    @enderror
                  </div>
                  <div class="col col-md-3 col-12 form-group">
                    <label for="state">State</label>
                    <input type="text" value="{{old('state')}}" name="state" class="form-control" id="state" placeholder="Enter State">
                    @error('state')
                      <p class="text-danger">{{$errors->first('state')}}</p>
                    @enderror
                  </div>
                  <div class="col col-md-3 col-12 form-group">
                    <label for="country">Country</label>
                    <input type="text" value="{{old('country')}}" name="country" class="form-control" id="country" placeholder="Enter Country">
                    @error('country')
                      <p class="text-danger">{{$errors->first('country')}}</p>
                    @enderror
                  </div>
                  <div class="col col-md-3 col-12 form-group">
                    <label for="zip">Zip Code</label>
                    <input type="text" value="{{old('zip')}}" name="zip" class="form-control" id="zip" placeholder="Enter Zip Code">
                    @error('zip')
                      <p class="text-danger">{{$errors->first('zip')}}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">

        </div>
        <!--/.col (right) -->
      </div>
@endsection
