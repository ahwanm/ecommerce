@extends('layouts.admin')

@section('title', 'Edit Products')

@section('header-content')
<!-- Select2 -->
<link rel="stylesheet" href="/admin-lte/plugins/select2/css/select2.min.css">
<style media="screen">
  .gg{
    background-color: red;
  }
</style>
@endsection

@section('content-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Edit Product</h1>
    </div>
  </div>
</div>
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-9">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Enter Product Details</h3>
            </div>
            <form id="quickForm" method="POST" action="/admin/products/{{$product->id}}">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col form-group col-md-2">
                    <label for="productSlug">Slug</label>
                    <input readonly value="{{ $errors->any() && old('productSlug')!=null ? old('productSlug'):$product->slug}}" type="text" name="productSlug" class="form-control" id="productSlug" placeholder="Slug">
                  </div>
                  <div class="col form-group col-md-10">
                    <label for="productName">Product Name</label>
                    <input type="text" name="productName" class="form-control" id="productName" placeholder="Enter product name" value="{{ $errors->any() && old('productName')!=null? old('productName'):$product->name}}">
                    @error('productName')
                      <p class="text-danger">{{$errors->first('productName')}}</p>
                    @enderror
                  </div>
                </div>
                <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="productDescription" rows="3" placeholder="Enter product description">{{ $errors->any() && old('productDescription')!=null ? old('productDescription'):$product->description}}</textarea>
                        @error('productDescription')
                          <p class="text-danger">{{$errors->first('productDescription')}}</p>
                        @enderror
                </div>
                <div class="row">
                  <div class="col form-group">
                    <label for="productPrice">Price</label>
                    <input type="text" name="productPrice" class="form-control" id="productPrice" placeholder="Price" value="{{ $errors->any() && old('productPrice')!=null ? old('productPrice'):$product->price}}">
                    @error('productPrice')
                      <p class="text-danger">{{$errors->first('productPrice')}}</p>
                    @enderror
                  </div>
                  <div class="col form-group">
                    <label for="productQuantity">Quantity</label>
                    <input value="{{ $errors->any() && old('productQuantity')!=null ? old('productQuantity'):$product->quantity}}" type="text" name="productQuantity" class="form-control" id="productQuantity" placeholder="Quantity">
                    @error('productQuantity')
                      <p class="text-danger">{{$errors->first('productQuantity')}}</p>
                    @enderror
                  </div>
                  <div class="col form-group">
                    <label for="productSku">Sku</label>
                    <input readonly value="{{ $errors->any() && old('productSku')!=null ? old('productSku'):$product->sku}}" type="text" name="productSku" class="form-control" id="productSku" placeholder="Sku">
                    @error('productSku')
                      <p class="text-danger">{{$errors->first('productSku')}}</p>
                    @enderror
                  </div>
                  <div class="col form-group">
                    <label>Category</label>
                    <select class="select2" name="categories[]" id="categories" multiple="multiple" data-placeholder="Select categories" style="width: 100%;">
                      @foreach ($categories as $category)
                        <option
                        @if (old('categories') != null && in_array( $category->id, old('categories'))){{"selected"}} @endif
                        @if(old('categories') == null && in_array( $category->id, $categoryMap)){{"selected"}}@endif value="{{ $category->id }}">{{ $category->categoryname }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update Product</button>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-3">
          <form id="quickForm" method="POST" action="/admin/products/{{$product->id}}/imageReplace" enctype="multipart/form-data">
            @csrf
            <div class="card card-default">
              <div class="card-header bg-primary">
                <h3 class="card-title text-light">Product Image</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body" style="display: block;">
                <div class="image-upload">
                  <label for="file-input">
                    <img  width="100%" src="{{ $product->getFirstMediaUrl('productImages','thumb') }}" class="attachment-266x266 size-266x266" alt="" loading="lazy" srcset="" sizes="(max-width: 266px) 100vw, 266px">
                  </label>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" class="form-control" name="productImage" id="productImage" required>
                      <label class="custom-file-label" for="productImage">Choose Product Image</label>
                    </div>
                  </div>
                  @error('productImage')
                    <p class="text-danger">{{$errors->first('productImage')}}</p>
                  @enderror
                </div>
              </div>
              <div class="card-footer" style="display: block;">
                <button type="submit" class="btn btn-primary">Replace Image</button>
              </div>
            </div>
          </form>
          <!--<form id="quickForm" method="POST" action="/admin/products/{{$product->id}}/addToGallery" enctype="multipart/form-data">
            @csrf
            <div class="card card-primary collapsed-card">
              <div class="card-header">
                <h3 class="card-title">Product Gallery</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="row mb-3">
                  @foreach ($product->getMedia('productGallery') as $galleryImage)
                  <div class="col-sm-2 ">
                    <a href="" data-toggle="lightbox" data-title="sample 1 - white" data-gallery="gallery">
                      <img src="{{$galleryImage->getUrl()}}" class="border img-fluid mb-2" alt="white sample">
                    </a>
                  </div>
                  @endforeach
                </div>
                <div class="image-upload">
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" class="form-control" name="productGallery" id="productGallery" required>
                      <label class="custom-file-label" for="productGallery">Choose Product Image</label>
                    </div>
                  </div>
                  @error('productGallery')
                    <p class="text-danger">{{$errors->first('productGallery')}}</p>
                  @enderror
                </div>
              </div>
              <div class="card-footer" >
                <button type="submit" class="btn btn-primary">Add To Gallery</button>
              </div>
            </div>
          </form> -->
        </div>
      </div>
    </div>
  </section>
@endsection

@section('script-content')
  <!-- Select2 -->
  <script src="/admin-lte/plugins/select2/js/select2.full.min.js"></script>
  <script src="/admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
  <script>
    $('.select2').select2();
    $('#productImage').on('change',function(){
              var fileName = $(this).val().split("\\").pop();
              //replace the "Choose a file" label
              $(this).next('.custom-file-label').html(fileName);
    })
  </script>
@endsection
