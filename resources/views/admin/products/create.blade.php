@extends('layouts.admin')

@section('title', 'Add Product')

@section('header-content')
  <!-- Select2 -->
  <link rel="stylesheet" href="/admin-lte/plugins/select2/css/select2.min.css">
@endsection

@section('content-header')
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Add Product</h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
@endsection

@section('content')
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Enter Product Details</h3>
            </div>
            <form id="quickForm" method="POST" action="/admin/products" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <div class="form-group">
                  <label for="productName">Product Name</label>
                  <input type="text" name="productName" value="{{old('productName')}}" class="form-control" id="productName" placeholder="Enter product name">
                  @error('productName')
                    <p class="text-danger">{{$errors->first('productName')}}</p>
                  @enderror
                </div>
                <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" name="productDescription" rows="3" placeholder="Enter product description">{{old('productDescription')}}</textarea>
                  @error('productDescription')
                    <p class="text-danger">{{$errors->first('productDescription')}}</p>
                  @enderror
                </div>
                <div class="row">
                  <div class="col col-6 col-md-3 form-group">
                      <label for="productPrice">Price</label>
                      <input type="text" value="{{old('productPrice')}}" name="productPrice" class="form-control" id="productPrice" placeholder="Price">
                      @error('productPrice')
                        <p class="text-danger">{{$errors->first('productPrice')}}</p>
                      @enderror
                    </div>
                  <div class="col col-6 col-md-3 form-group">
                      <label for="productQuantity">Quantity</label>
                      <input type="text" name="productQuantity" value="{{old('productQuantity')}}" class="form-control" id="productQuantity" placeholder="Quantity">
                      @error('productQuantity')
                        <p class="text-danger">{{$errors->first('productQuantity')}}</p>
                      @enderror
                    </div>
                  <div class="col col-12 col-md-3 form-group">

                      <label>Category</label>
                      <select class="select2" name="categories[]" id="categories" multiple="multiple" data-placeholder="Select categories" style="width: 100%;">
                        @foreach ($categories as $category)
                          <option @if(old('categories') != null && in_array( $category->id, old('categories'))){{"selected"}} @endif value="{{ $category->id }}">{{ $category->categoryname }}</option>
                        @endforeach
                      </select>
                    </div>
                  <div class="col col-12 col-md-3 form-group">
                        <label for="productImage">Product Image</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <!-- <input type="file" class="form-control-file" id="exampleFormControlFile1"> -->
                            <input type="file" class="form-control" name="productImage" id="productImage" required>
                            <label class="custom-file-label" for="productImage">Choose Product Image</label>
                          </div>
                        </div>
                        @error('productImage')
                          <p class="text-danger">{{$errors->first('productImage')}}</p>
                        @enderror
                      </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Add Product</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('script-content')

  <script src="/admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
  <!-- Select2 -->
  <script src="/admin-lte/plugins/select2/js/select2.full.min.js"></script>
  <script>
    $('.select2').select2();
    $('#productImage').on('change',function(){
              var fileName = $(this).val().split("\\").pop();
              //replace the "Choose a file" label
              $(this).next('.custom-file-label').html(fileName);
    })
  </script>

@endsection
