<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/cart', function (){
    return view('site.cart');
});

Route::get('/address', function (){
    return view('site.address');
});

Route::get('/checkout', function (){
    return view('site.checkout');
});

Route::get('/complete', function (){
    return view('site.complete');
});

Route::get('/contact', function (){
    return view('site.contact');
});




Route::get('users', ['uses'=>'UserController@index', 'as'=>'users.index']);

Route::get('admin/trash', 'TrashController@trash')->name('admin-trash')->middleware('admin');
Route::get('admin/trash/user', 'TrashController@userTrash')->name('admin-trash-user')->middleware('admin');
Route::get('admin/trash/product', 'TrashController@productTrash')->name('admin-trash-product')->middleware('admin');
Route::get('admin/trash/category', 'TrashController@categoryTrash')->name('admin-trash-category')->middleware('admin');

Route::get('admin/products', 'ProductController@index')->name('admin-products')->middleware('admin');
Route::post('admin/products', 'ProductController@store')->name('admin-products-store')->middleware('admin');
Route::get('admin/products/create', 'ProductController@create')->name('admin-products-create')->middleware('admin');
Route::get('admin/products/{product}/edit', 'ProductController@edit')->name('admin-products-edit')->middleware('admin');
Route::post('admin/products/{product}', 'ProductController@update')->name('admin-products-update')->middleware('admin');
Route::get('admin/products/{id}/delete', 'ProductController@destroy')->name('admin-products-destroy')->middleware('admin');
Route::post('admin/products/{id}/imageReplace', 'ProductController@imageReplace')->name('admin-products-imageReplace')->middleware('admin');
Route::get('admin/products/{id}/restore', 'ProductController@restore')->name('admin-products-restore')->middleware('admin');
Route::get('admin/products/{id}/permanentdelete', 'ProductController@delete')->name('admin-products-delete')->middleware('admin');
// Route::post('/admin/products/{id}/addToGallery', 'ProductController@addToGallery')->name('admin-products-addToGallery')->middleware('admin');

Route::get('admin/users', 'UserController@index')->name('admin-users')->middleware('admin');
Route::get('admin/users/create', 'UserController@create')->name('admin-users-create');
Route::post('admin/users', 'UserController@store')->name('admin-users-store');
Route::get('admin/users/{customer}/edit', 'UserController@edit')->name('admin-users-edit')->middleware('admin');
Route::post('admin/users/{customer}', 'UserController@update')->name('admin-users-update')->middleware('admin');
Route::get('admin/users/{id}/delete', 'UserController@destroy')->name('admin-users-destroy')->middleware('admin');
Route::get('admin/users/{id}/restore', 'UserController@restore')->name('admin-users-restore')->middleware('admin');
Route::get('admin/users/{id}/permanentdelete', 'UserController@delete')->name('admin-users-delete')->middleware('admin');

Route::get('admin/categories', 'CategoryController@index')->name('admin-categories')->middleware('admin');
Route::get('admin/categories/create', 'CategoryController@create')->name('admin-categories-create')->middleware('admin');
Route::post('admin/categories', 'CategoryController@store')->name('admin-categories-store')->middleware('admin');
Route::get('admin/categories/{id}/edit', 'CategoryController@edit')->name('admin-categories-edit')->middleware('admin');
Route::post('admin/categories/{id}', 'CategoryController@update')->name('admin-categories-update')->middleware('admin');
Route::get('admin/categories/{id}/delete', 'CategoryController@destroy')->name('admin-categories-destroy')->middleware('admin');
Route::post('admin/categories/{id}/imageReplace', 'CategoryController@imageReplace')->name('admin-categories-imageReplace')->middleware('admin');
Route::get('admin/categories/{id}/restore', 'CategoryController@restore')->name('admin-categories-restore')->middleware('admin');
Route::get('admin/categories/{id}/permanentdelete', 'CategoryController@delete')->name('admin-categories-delete')->middleware('admin');

Auth::routes();
Route::get('admin', 'UserController@dashboard')->name('admin-dashboard')->middleware('admin');
Route::get('admin/dashboard', 'UserController@dashboard')->name('admin-dashboard')->middleware('admin');

Route::get('/product/{slug}/{sku}', 'SiteController@item')->name('site-item');
Route::get('/category/{parent}/{slug}', 'SiteController@subcategory')->name('site-child-category');
Route::get('/category/{slug}', 'SiteController@category')->name('site-parent-category');
Route::get('/shop', 'SiteController@shop')->name('site-shop');
Route::post('/contact', 'SiteController@sendEmail')->name('site-contact');
Route::get('/dashboard', 'SiteController@dashboard')->name('site-dashboard')->middleware('auth');


Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/', 'SiteController@homePage')->name('site-home');
