<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\Category;
use App\Models\CategoryProductMap;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Auth;
use DataTables;
Use Alert;

function generatesku() {
  $number = mt_rand(1000000000, 9999999999);
  if (skuExists($number)) {
      return generatesku();
  }
  return $number;
}
function skuExists($number)
{
  return Product::wheresku($number)->exists();
}
class ProductController extends Controller
{

    public function index(Request $request)
    {
      if($request->msg!="")
      {
        toast($request->msg,'success');
      }
      if ($request->ajax()) {
          if($request["categories"]!="none")
          {
              $data = Category::find($request["categories"])->products;
          }
          else {
            $data = Product::latest()->get();
          }
          return Datatables::of($data)
                  ->addIndexColumn()
                  ->addColumn('creator', function($row){
                    return $row->creator->name;
                  })
                  ->addColumn('image', function($row){
                    $url = $row->getFirstMediaUrl('productImages');
                    return '<img src='.$url.' alt="" height="50" width="80" style="object-fit:cover" />';
                  })
                  ->addColumn('action', function($row){
                         $btn = '<a href="/admin/products/'.$row['id'] .'/edit" class="edit btn btn-primary btn-sm">Edit</a>';
                         $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/products/'.$row['id'] .'/delete" class="delete btn btn-danger btn-sm">Delete</a>';
                          return $btn;
                  })
                  ->rawColumns(['action', 'image'])
                  ->make(true);
      }
      $msg = ($request->msg) ?  $request->msg :"";
      if($request["categories"])
      {
          return view('admin.products.index', ["fetchCat"=> $request["categories"], "msg"=>$msg]);
      }
      return view('admin.products.index', ["fetchCat"=> "none", "msg"=>$msg]);
    }
    public function show($id)
    {
      $product = Product::find($id);
      return view('admin.products.show',['product' => $product]);
    }
    public function create()
    {
      $data = Category::all();
      return view('admin.products.create',['categories' => $data]);
    }
    public function edit($id)
    {
      $product = Product::find($id);
      $data = Category::all();
      $categoryMap = $product->categories->pluck('id')->toArray();
      return view('admin.products.edit', ['product' => $product, 'categoryMap' => $categoryMap, 'categories' => $data] );
    }
    public function store()
    {
      request()->validate([
          'productName' => ['required', 'max:100', 'min:3'],
          'productDescription' => ['required', 'min:5'],
          'productImage' => ['required', 'max:2048', 'image'],
          'productPrice' => ['required', 'numeric'],
          'productQuantity' => ['required', 'integer'],
          'categories' => ['exists:categories,id']
      ]);
      $product = new Product();
      // dd($_FILES);
      $product->name = request('productName');
      $product->description = request('productDescription');
      $product->price = request('productPrice');
      $product->sku = generatesku();
      $product->quantity = request('productQuantity');
      $product->slug = str_slug(request('productName'), "-");
      $product->createdby = Auth::user()->id;
      $product->save();
      $product->addMediaFromRequest('productImage')->toMediaCollection('productImages');
      $category_ids = request()->categories;
      $product->categories()->sync($category_ids);
      return redirect()->route('admin-products', ["msg"=>"Product created successfully."]);
    }
    public function imageReplace($id)
    {
      request()->validate([
          'productImage' => ['required', 'max:2048', 'image'],
      ]);
      $product = Product::find($id);
      $product->clearMediaCollection('productImages');
      $product->addMediaFromRequest('productImage')->toMediaCollection('productImages');
      return \Redirect::route('admin-products-edit', [$id]);
    }
    // public function addToGallery($id)
    // {
    //   request()->validate([
    //       'productGallery' => ['required', 'max:2048', 'image'],
    //   ]);
    //   $product = Product::find($id);
    //   $product->addMediaFromRequest('productGallery')->toMediaCollection('productGallery');
    //   return \Redirect::route('admin-products-edit', [$id]);
    // }
    public function update($id)
    {
      request()->validate([
          'productName' => ['required', 'max:100', 'min:3'],
          'productDescription' => ['required', 'min:5'],
          'productPrice' => ['required', 'numeric'],
          'productQuantity' => ['required', 'integer'],
          'productSku' => ['required', 'unique:products,sku,'.$id, 'digits:10'],
      ]);
      $categories = request()->categories;
      $product = Product::find($id);
      $product->name = request('productName');
      $product->description = request('productDescription');
      $product->quantity = request('productQuantity');
      $product->sku = request('productSku');
      $product->price = request('productPrice');
      $product->slug = str_slug(request('productName'), "-");
      $product->save();
      $category_ids = request()->categories;
      $product->categories()->sync($category_ids);
      return redirect()->route('admin-products', ["msg"=>"Product updated."]);
    }
    public function destroy($id)
    {
      $product = Product::find($id)->delete();
      return redirect()->route('admin-products', ["msg"=>"Product deleted."]);
    }
    public function restore($id)
    {
      $product = Product::withTrashed()->find($id);
      $product->restore();
      $msg="Product Restoration Successfull.";
      return redirect()->route('admin-trash', ["msg" =>$msg, '#product-content']);
    }
    public function delete($id)
    {
      $product = Product::withTrashed()->find($id);
      $product->forceDelete();
      $msg="Product Deleted Permanetly.";
      return redirect()->route('admin-trash', ["msg" =>$msg, '#product-content']);
    }
}
