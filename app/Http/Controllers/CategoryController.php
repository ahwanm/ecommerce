<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\CategoryProductMap;
use Illuminate\Http\UploadedFile;
use DataTables;
Use Alert;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
      if($request->msg!="")
      {
        toast($request->msg,'success');
      }
      if ($request->ajax()) {
          $data = Category::latest()->get();
          return Datatables::of($data)
                  ->addIndexColumn()
                  ->addColumn('image', function($row){
                    $url = $row->getFirstMediaUrl('categoryImages');
                    return '<img src='.$url.' alt="" height="50" width="80" style="object-fit:cover" />';
                  })
                  ->addColumn('parent', function($row){
                    if($row->parent)
                    {
                      return $row->parent->categoryname;
                    }
                    return "";
                  })
                  ->addColumn('action', function($row){
                         $btn = '<a href="/admin/categories/'.$row['id'] .'/edit" class="edit btn btn-primary btn-sm">Edit</a>';
                         $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/categories/'.$row['id'] .'/delete" class="delete btn btn-danger btn-sm">Delete</a>';
                         $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/products?categories='.$row['id'] .'" class="delete btn btn-warning btn-sm">View Products</a>';
                         return $btn;
                  })
                  ->rawColumns(['action', 'image'])
                  ->make(true);
      }
      $msg = ($request->msg) ?  $request->msg :"";
      return view('admin.categories.index', ["msg"=>$msg]);
    }
    public function create()
    {
      $categories = Category::all();
      return view('admin.categories.create', ["categories"=>$categories]);
    }
    public function store()
    {
      request()->validate([
          'name' => ['required', 'max:255', 'min:2', 'unique:categories,categoryname'],
          'categoryDescription' => ['required', 'min:5'],
          'categoryImage' => ['required', 'max:2048', 'image'],
      ]);
      $category = new Category();
      $category->categoryname = request()->name;
      $category->slug = str_slug(request()->name, "-");
      $category->description = request()->categoryDescription;
      if(request()->parentCategory!="")
      {
        $category->parent_id = request()->parentCategory;
      }
      $category->save();
      $category->addMediaFromRequest('categoryImage')->toMediaCollection('categoryImages');
      return redirect()->route('admin-categories', ["msg"=>"Category created successfully."]);
    }
    public function edit($id)
    {
      $category = Category::find($id);
      $categories = Category::all();
      return view('admin.categories.edit', ['category'=>$category, 'categories'=>$categories]);
    }
    public function update($id)
    {
      request()->validate([
          'name' => ['required', 'max:255', 'min:2', 'unique:categories,categoryname,'.$id],
          'categoryDescription' => ['required', 'min:5'],
      ]);
      $category = Category::find($id);
      $category->categoryname = request()->name;
      $category->description = request()->categoryDescription;
      $category->slug = str_slug(request()->name, "-");
      if(request()->parentCategory!="")
      {
        $category->parent_id = request()->parentCategory;
      }
      else {
        $category->parent_id = null;
      }
      $category->save();
      return redirect()->route('admin-categories', ["msg"=>"Category edited successfully."]);
    }
    public function destroy($id)
    {
      $category = Category::find($id)->delete();
      return redirect()->route('admin-categories', ["msg"=>"Category deleted successfull."]);
    }
    public function imageReplace($id)
    {
      request()->validate([
          'categoryImage' => ['required', 'max:1024', 'image'],
      ]);
      $category = Category::find($id);
      $category->clearMediaCollection('categoryImages');
      $category->addMediaFromRequest('categoryImage')->toMediaCollection('categoryImages');
      return \Redirect::route('admin-categories-edit', [$id]);
    }
    public function restore($id)
    {
      $category = Category::withTrashed()->find($id);
      $category->restore();
      $msg="Category Restoration Successfull.";
      return redirect()->route('admin-trash', ["msg" =>$msg, '#category-content']);
    }
    public function delete($id)
    {
      $category = Category::withTrashed()->find($id);
      $category->forceDelete();
      $msg="Category Deleted Permanetly.";
      return redirect()->route('admin-trash', ["msg" =>$msg, '#category-content']);
    }
}
