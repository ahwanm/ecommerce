<?php

namespace App\Http\Controllers;
use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
      $customers = Customer::all();
      return view('admin.users.index', ['customers'=>$customers]);
    }
    public function create()
    {
      return view('admin.users.create');
    }
    public function store()
    {
      $customer = new Customer();
      $customer->name = request()->name;
      $customer->email = request()->email;
      $customer->phone = request()->phone;
      $customer->status = request()->status;
      $customer->save();
      return redirect('/admin/customers');
    }
    public function edit($id)
    {
      $customer = Customer::find($id);
      return view('admin.users.edit', ['customer'=>$customer]);
    }
    public function update($id)
    {
      $customer = Customer::find($id);
      $customer->name = request()->name;
      $customer->email = request()->email;
      $customer->phone = request()->phone;
      $customer->status = request()->status;
      $customer->save();
      return redirect('/admin/customers');
    }
}
