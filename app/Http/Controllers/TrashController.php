<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\Product;
use App\Models\Category;
use DataTables;
Use Alert;

use Illuminate\Http\Request;

class TrashController extends Controller
{
    public function categoryTrash(Request $request)
    {
      if($request->msg!="")
      {
        toast($request->msg,'success');
      }
      if ($request->ajax()) {
          $data = Category::onlyTrashed()->get();
          return Datatables::of($data)
                  ->addIndexColumn()
                  ->addColumn('image', function($row){
                    $url = $row->getFirstMediaUrl('categoryImages');
                    return '<img src='.$url.' alt="" height="50" width="80" style="object-fit:cover" />';
                  })
                  ->addColumn('action', function($row){
                         $btn = '<a href="/admin/categories/'.$row['id'] .'/restore" class="edit btn btn-primary btn-sm">Restore</a>';
                         $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/categories/'.$row['id'] .'/permanentdelete" class="delete btn btn-danger btn-sm">Delete</a>';
                         return $btn;
                  })
                  ->rawColumns(['action', 'image'])
                  ->make(true);
      }
    }
    public function productTrash(Request $request)
    {
      if ($request->ajax())
      {
          $data = Product::onlyTrashed()->get();
          return Datatables::of($data)
                  ->addIndexColumn()
                  ->addColumn('creator', function($row){
                    return $row->creator->name;
                  })
                  ->addColumn('image', function($row){
                    $url = $row->getFirstMediaUrl('productImages');
                    return '<img src='.$url.' alt="" height="50" width="80" style="object-fit:cover" />';
                  })
                  ->addColumn('action', function($row){
                         $btn = '<a href="/admin/products/'.$row['id'] .'/restore" class="edit btn btn-primary btn-sm">Restore</a>';
                         $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/products/'.$row['id'] .'/permanentdelete" class="delete btn btn-danger btn-sm">Delete</a>';
                          return $btn;
                  })
                  ->rawColumns(['action', 'image'])
                  ->make(true);
      }
    }
    public function userTrash(Request $request)
    {
        if ($request->ajax()) {
            $data = User::onlyTrashed()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<a href="/admin/users/'.$row['id'] .'/restore" class="edit btn btn-primary btn-sm">Restore</a>';
                           $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/users/'.$row['id'] .'/permanentdelete" class="delete btn btn-danger btn-sm">Delete</a>';
                           return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }
    public function trash(Request $request)
    {
        if($request->msg!="")
        {
          toast($request->msg,'success');
        }
        $msg = ($request->msg) ?  $request->msg :"";
        return view('admin.trash', ["msg"=>$msg]);
    }
}
