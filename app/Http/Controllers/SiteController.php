<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Mail;
use Auth;

class SiteController extends Controller
{
    public function dashboard()
    {
      $user = Auth::user();
      return view('site.dashboard', ['user'=>$user]);
    }
    public function homePage()
    {
      $count = 2;
      $maxPrice = Product::max('price');
      $products = Product::where('featured','=','1');
      $q=[];
      if(request()->has('category'))
      {
        $products = new Category;
        $products = $products->find(request()->category)->products()->where('featured','=','1');
        $q['category']=request()->category;
      }
      if(request()->has('priceLow'))
      {
        $priceHigh = explode(' ', request()->priceHigh)[1];
        $priceLow = explode(' ', request()->priceLow)[1];
        $q['priceLow']=$priceLow;
        $q['priceHigh']=$priceHigh;
        $products = $products->where('price', '<=', $priceHigh)->where('price', '>=', $priceLow);
      }
      $products = $products->paginate($count)->appends($q);
      $categories = Category::whereNull('parent_id')->get();
      $latestProducts = Product::latest()->take(4)->get();
      return view('site.welcome', ['latestProducts'=>$latestProducts, 'products'=>$products, 'parentCategories'=>$categories, 'count'=>$count, 'maxPrice'=>$maxPrice]);
    }
    public function sendEmail()
    {
      request()->validate([
          'email' => ['required', 'email'],
          'name' => ['required', 'min:3'],
          'subject' => ['required'],
          'message' => ['required', 'min:3'],
          'phone' => ['required', 'digits:10'],
      ]);
      Mail::raw(request()->message, function($message) {
          $message->from(request()->email, request()->name);
          $message->to('cse.170310069@silicon.ac.in');
          $message->subject(request()->subject);
      });
      return redirect()->route('site-home');
    }
    public function item($slug, $sku)
    {
      $product = Product::where('sku', '=',$sku)->firstOrFail();
      $categories = Category::whereNull('parent_id')->get();
      return view('site.item',['product'=>$product, 'parentCategories'=>$categories]);
    }
    public function shop()
    {
      $count = 9;
      $maxPrice = Product::max('price');
      $products = new Product;
      $q=[];
      if(request()->has('category'))
      {
        $products = new Category;
        $products = $products->find(request()->category)->products();
        $q['category']=request()->category;
      }
      if(request()->has('priceLow'))
      {
        $priceHigh = explode(' ', request()->priceHigh)[1];
        $priceLow = explode(' ', request()->priceLow)[1];
        $q['priceLow']=$priceLow;
        $q['priceHigh']=$priceHigh;
        $products = $products->where('price', '<=', $priceHigh)->where('price', '>=', $priceLow);
      }
      $products = $products->paginate($count)->appends($q);
      $categories = Category::whereNull('parent_id')->get();
      return view('site.shop',['products'=>$products, 'parentCategories'=>$categories, 'count'=>$count, 'maxPrice'=>$maxPrice]);
    }
    public function category($slug)
    {
      $count = 3;
      $maxPrice = Product::max('price');
      $q=[];
      $category = Category::where('slug', '=',$slug)->firstOrFail();
      $categoriesSideBar = Category::whereNull('parent_id')->get();
      $products = $category->products();
      if(request()->has('subcategory'))
      {
        $products = new Category;
        $products = $products->find(request()->subcategory)->products();
        $q['category']=request()->subcategory;
      }
      if(request()->has('priceLow'))
      {
        $priceHigh = explode(' ', request()->priceHigh)[1];
        $priceLow = explode(' ', request()->priceLow)[1];
        $q['priceLow']=$priceLow;
        $q['priceHigh']=$priceHigh;
        $products = $products->where('price', '<=', $priceHigh)->where('price', '>=', $priceLow);
      }
      $products = $products->paginate($count)->appends($q);

      return view('site.category', ['category'=>$category, 'parentCategories'=>$categoriesSideBar, 'count'=>$count, 'maxPrice'=>$maxPrice, 'products'=>$products]);
    }
    public function subcategory( $parent, $slug)
    {
      $count = 3;
      $maxPrice = Product::max('price');
      $q=[];
      $category = Category::where('slug', '=',$slug)->firstOrFail();
      $categoriesSideBar = Category::whereNull('parent_id')->get();
      $products = $category->products();
      if(request()->has('subcategory'))
      {
        $products = new Category;
        $products = $products->find(request()->subcategory)->products();
        $q['category']=request()->subcategory;
      }
      if(request()->has('priceLow'))
      {
        $priceHigh = explode(' ', request()->priceHigh)[1];
        $priceLow = explode(' ', request()->priceLow)[1];
        $q['priceLow']=$priceLow;
        $q['priceHigh']=$priceHigh;
        $products = $products->where('price', '<=', $priceHigh)->where('price', '>=', $priceLow);
      }
      $products = $products->paginate($count)->appends($q);

      return view('site.category', ['category'=>$category, 'parentCategories'=>$categoriesSideBar, 'count'=>$count, 'maxPrice'=>$maxPrice, 'products'=>$products]);
    }
}
