<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\Product;
use App\Models\UserAddress;
use App\Models\Category;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
Use Alert;

class UserController extends Controller
{
    public function index(Request $request)
    {
      if($request->msg!="")
      {
        toast($request->msg,'success');
      }
      if ($request->ajax()) {
          $data = User::latest()->get();
          return Datatables::of($data)
                  ->addIndexColumn()
                  ->addColumn('action', function($row){
                         $btn = '<a href="/admin/users/'.$row['id'] .'/edit" class="edit btn btn-primary btn-sm">Edit</a>';
                         $rid = $row->id;
                         $r = "/admin/users/" . $rid . "/delete";
                         $btn .= '&nbsp;&nbsp;&nbsp;<button class="btn btn-danger btn-flat btn-sm remove-user" data-id="'.$rid.'" data-action="'.$r.'"> Delete</button>';
                         // $btn .= '&nbsp;&nbsp;&nbsp;<a href="/admin/users/'.$row['id'] .'/delete" class="delete btn btn-danger btn-sm">Delete</a>';
                         return $btn;
                  })
                  ->rawColumns(['action'])
                  ->make(true);
      }
      $msg = ($request->msg) ?  $request->msg :"";
      return view('admin.users.index', ["msg"=>$msg]);
    }
    public function dashboard(){
      return view('admin.dashboard',['productCount'=>count(Product::all()), 'userCount'=>count(User::all()), 'categoryCount'=>count(Category::all())]);
    }
    public function login(){
      return view('admin.login');
    }
    public function create()
    {
      return view('admin.users.create');
    }
    public function store()
    {
      request()->validate([
          'name' => ['required', 'max:255', 'min:3'],
          'password' => ['required', 'min:5', 'max:255'],
          'email' => ['required', 'email', 'max:255', 'unique:users,email'],
          'type' => ['required'],
          'phone' => ['required', 'digits:10'],
          'zip' => ['required', 'digits:6'],
          'state' => ['required', 'max:255', 'min:3'],
          'city' => ['required', 'max:255', 'min:3'],
          'country' => ['required', 'max:255', 'min:3'],
          'address' => ['required', 'min:5'],
      ]);
      $user = new User();
      $user->name = request()->name;
      $user->password = Hash::make(request()->password);
      $user->email = request()->email;
      $user->phone = request()->phone;
      $user->type = request()->type;
      $user->save();
      $userAddress = new UserAddress;
      $userAddress->zip = request()->zip;
      $userAddress->country = request()->country;
      $userAddress->state = request()->state;
      $userAddress->city = request()->city;
      $userAddress->address = request()->address;
      $userAddress->user_id = $user->id;
      $userAddress->save();
      return redirect()->route('admin-users', ["msg"=>"User creation successfull."]);
    }
    public function edit($id)
    {
      $user = User::find($id);
      return view('admin.users.edit', ['user'=>$user]);
    }
    public function update($id)
    {
      request()->validate([
          'name' => ['required', 'max:255', 'min:3'],
          'email' => ['required', 'email', 'max:255', 'unique:users,email,'.$id],
          'type' => ['required'],
          'phone' => ['required', 'digits:10'],
          'zip' => ['required', 'digits:6'],
          'state' => ['required', 'max:255', 'min:3'],
          'city' => ['required', 'max:255', 'min:3'],
          'country' => ['required', 'max:255', 'min:3'],
          'address' => ['required', 'min:5'],
      ]);

      $user = User::find($id);
      $user->name = request()->name;
      $user->email = request()->email;
      $user->phone = request()->phone;
      $user->type = request()->type;
      $user->save();
      $userAddress = new UserAddress;
      $userAddress->zip = request()->zip;
      $userAddress->country = request()->country;
      $userAddress->state = request()->state;
      $userAddress->city = request()->city;
      $userAddress->address = request()->address;
      $userAddress->user_id = $user->id;
      $userAddress->save();
      return redirect()->route('admin-users', ["msg"=>"Update successfull."]);
    }
    public function destroy($id)
    {
      $user = User::find($id);
      $msg="Cannot delete active user.";
      if($user->id != Auth::user()->id)
      {
        $user->delete();
        $msg="Delete successfull.";
      }
      return redirect()->route('admin-users', ["msg" =>$msg]);
    }
    public function restore($id)
    {
      $user = User::withTrashed()->find($id);
      $user->restore();
      $msg="User Restoration Successfull.";
      return redirect()->route('admin-trash', ["msg" =>$msg, '#user-content']);
    }
    public function delete($id)
    {
      $user = User::withTrashed()->find($id);
      $user->forceDelete();
      $msg="User Deleted Permanetly.";
      return redirect()->route('admin-trash', ["msg" =>$msg, '#user-content']);
    }
}
