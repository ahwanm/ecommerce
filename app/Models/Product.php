<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model implements HasMedia
{
    use HasMediaTrait;
    use SoftDeletes;
    public function creator()
    {
          return $this->belongsTo('App\User', 'createdby', 'id');
    }
    public function categories()
    {
          return $this->belongsToMany('App\Models\Category', 'category_product_maps', 'product_id', 'category_id');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
              ->width(270)
              ->height(370)
              ->crop('crop-center', 270, 370);
    }
}
