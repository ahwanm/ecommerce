<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    const ADMIN_TYPE = 'admin';
    const MANAGER_TYPE = 'user';

    public function isAdmin() {
         return $this->type === self::ADMIN_TYPE;
     }

    public function isUser() {
         return $this->type === self::MANAGER_TYPE;
    }

    public function products()
    {
          return $this->hasMany('App\Models\Product', 'createdby', 'id' );
    }
    public function addresses()
    {
          return $this->hasMany('App\Models\UserAddress', 'user_id', 'id' );
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type'  , 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
